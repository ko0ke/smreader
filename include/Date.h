#ifndef _date_h_
#define _date_h_

#include "Helper.h"

#include <vector>
#include <string>
#include <sstream>
#include <chrono>
#include <tuple>

class Date {

    private:

        std::vector<std::tuple<int,int>> vDays;

        Date();

        int getDay(int);

    public:

        static std::vector<unsigned char> strDateToHex(std::string);

        static std::string hexDateToStr(std::vector<unsigned char>);

        static int dayOfTheWeek(int , int , int );

        static std::string timestamp();

        static std::string now();

        static int compare(std::string, std::string);

        static std::string nextDateToDownload(std::string, int);

};

#endif