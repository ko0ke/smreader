#ifndef _file_h_
#define _file_h_

#include <string>
#include <fstream>

using namespace std;

class File
{
    private:

        static const std::string HEADER_CURVE_CSV;
        static const std::string HEADER_CLOSURE_CSV;

        static bool checkFile(std::string);

        File();

    public:

        static void write(std::string, std::string);

};

#endif