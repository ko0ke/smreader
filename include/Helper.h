#ifndef _helper_h_
#define _helper_h_

#include <iostream>

#include <sstream>
#include <vector>
#include <string>
#include <stdexcept>
#include <sstream>
#include <iomanip>

class Helper {

    private:

        Helper();
        
    public:

        static std::string frameToString(std::vector<unsigned char>);

        static int stringToInt(std::string);

        static unsigned char intToHex(int);

        static unsigned char stringToHex(std::string);


};

#endif