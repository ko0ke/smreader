#ifndef _controller_h_
#define _controller_h_

#include <iostream>

#include "../Communication/include/Communication.h"
#include "../Communication/include/Serial.h"
#include "../Communication/include/Socket.h"
#include "Helper.h"
#include "../IEC-62056/include/DLMSController.h"
#include "../IEC-62056/src/DLMSController.cpp"

#include <vector>
#include <cstring>
#include <sstream>
#include <string>
#include <memory>

class Controller {

    private:

        std::vector<std::unique_ptr<Communication>> vCommunication;

        // General parameters
        std::string mode;
        // debugLevel
        std::string logFile;

        std::string getParameter(std::string);

    public:

        Controller();

        void configure(int argc, char *argv[]);


};

#endif