#ifndef _hdlc_h_
#define _hdlc_h_

#include "HDLC.h"
#include "../../include/Helper.h"
#include "../../include/File.h"

#include <iostream>
#include <vector>
#include <algorithm>

template <class T>
class DLMSController {

    private:

        static const int RETRIES;

        int code;
        int retryNumber;
        bool connected;
        bool associated;
        int capturePeriod;
        std::vector<unsigned char> vReceivedData;
        std::string strResults;

        HDLC hdlc;

        void extractCurve(int, T &);
        void extractClosure(int, T &);

        std::string strDateToStrFileName(std::string);
        
    public:

        DLMSController();

        void read(T &);

        void request(int, T &, std::string, std::string, std::string);

        std::vector<unsigned char> clean(std::vector<unsigned char>);

        int analyze(int, T &, std::vector<unsigned char>, std::string);

        std::string getResults();

};

#endif