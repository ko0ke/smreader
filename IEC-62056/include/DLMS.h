#ifndef _dlms_h_
#define _dlms_h_

#include "../../include/Helper.h"
#include "../../include/Date.h"

#include <iostream>
#include <vector>

class DLMS {

    private:

        // AARQ FLAG or association request
        static const unsigned char AARQ_FLAG;

        // Reference object system
        static const unsigned char LOGICAL_NAME; // DEFAULT
        static const unsigned char SHORT_NAME;
        static const unsigned char LOGICAL_NAME_CIPHERED;
        static const unsigned char SHORT_NAME_CIPHERED;

        // Authenticacion type
        static const unsigned char MORE_LOW;
        static const unsigned char LOW; // DEFAULT
        static const unsigned char HIGH;
        static const unsigned char MD5;
        static const unsigned char SHA1;
        static const unsigned char GMAC;  

        // Frame number
        unsigned int frameNumber;

        
        
    public:

        DLMS();

        std::vector<unsigned char> frame(std::string, std::string, std::string);
        
        void setFrameNumber(unsigned int);

};

#endif