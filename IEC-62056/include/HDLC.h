#ifndef _dlmscontroller_h_
#define _dlmscontroller_h_

#include "CRC.h"
#include "DLMS.h"

#include <iostream>

#include <vector>

class HDLC {

    private:

        // DEFAULT BYTE
        static const unsigned char DEFAULT_BYTE;

        // HDLC INITIAL AND FINAL FLAG FRAME
        static const unsigned char HDLC_FLAG;

        // HDLC FRAME TYPE
        static const unsigned char FRAME_TYPE_3;

        // STATIC HDLC FRAME LENGTH
        static const unsigned char STATIC_HDLC_FRAME_LENGTH;

        // HDLC CONTROL FIELDS (CLIENT)
        static const unsigned char CONNECTION_CONTROL_FIELD;
        static const unsigned char DISCONNECTION_CONTROL_FIELD;
        static const unsigned char FIRST_REQUEST_CONTROL_FIELD;
        static const unsigned char MAX_REQUEST_CONTROL_FIELD;

        // HDLC CONTROL FIELDS (METER)
        static const unsigned char UA_RESPONSE;

        // Last control field received
        unsigned char lastControlField;

        DLMS dlms;

        std::vector<unsigned char> calculateCRC(std::string, std::vector<unsigned char>);
        
    public:

        HDLC();

        std::vector<unsigned char> frame(std::string, std::string, std::string);

        void setLastControlField(unsigned char);

        void setFrameNumber(unsigned int);

};

#endif