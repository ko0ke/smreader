#include "../include/DLMS.h"

// AARQ FLAG or association request
const unsigned char DLMS::AARQ_FLAG = 0x60;

// Reference object system
const unsigned char DLMS::LOGICAL_NAME             = 0x01;
const unsigned char DLMS::SHORT_NAME               = 0x02;
const unsigned char DLMS::LOGICAL_NAME_CIPHERED    = 0x03;
const unsigned char DLMS::SHORT_NAME_CIPHERED      = 0x04;

// Authenticacion type
const unsigned char DLMS::MORE_LOW = 0x00;
const unsigned char DLMS::LOW      = 0x01;
const unsigned char DLMS::HIGH     = 0x02;
const unsigned char DLMS::MD5      = 0x03;
const unsigned char DLMS::SHA1     = 0x04;
const unsigned char DLMS::GMAC     = 0x05; 


DLMS::DLMS() {

}


std::vector<unsigned char> DLMS::frame(std::string requestType, std::string initialDate, std::string finalDate) {

    std::vector<unsigned char> vPDU;
    std::vector<unsigned char> vDate;

    // Flags that says is a DLMS request
    vPDU.push_back(0xE6);
    vPDU.push_back(0xE6);
    vPDU.push_back(0x00);

    // DLMS frame to associate with the meter
    if (requestType.compare("association") == 0) {

        // TODO: Añadir el password pasado por parametro

        vPDU.push_back(AARQ_FLAG);
        vPDU.push_back(0x00); // Frame length
        // Reference object system
        vPDU.push_back(0xA1);
        vPDU.push_back(0x09);
        vPDU.push_back(0x06);
        vPDU.push_back(0x07);
        vPDU.push_back(0x60);
        vPDU.push_back(0x85);
        vPDU.push_back(0x74);
        vPDU.push_back(0x05);
        vPDU.push_back(0x08);
        vPDU.push_back(0x01);
        vPDU.push_back(LOGICAL_NAME);
        // Security mechanism (Authentication type)
        vPDU.push_back(0x8A);
        vPDU.push_back(0x02);
        vPDU.push_back(0x07);
        vPDU.push_back(0x80);
        vPDU.push_back(0x8B);
        vPDU.push_back(0x07);
        vPDU.push_back(0x60);
        vPDU.push_back(0x85);
        vPDU.push_back(0x74);
        vPDU.push_back(0x05);
        vPDU.push_back(0x08);
        vPDU.push_back(0x02);
        vPDU.push_back(LOW);
        // Password
        vPDU.push_back(0xAC);
        vPDU.push_back(0x0A); // Field length
        vPDU.push_back(0x80);
        vPDU.push_back(0x08); // Password length
        vPDU.push_back(0x30);
        vPDU.push_back(0x30);
        vPDU.push_back(0x30);
        vPDU.push_back(0x30);
        vPDU.push_back(0x30);
        vPDU.push_back(0x30);
        vPDU.push_back(0x30);
        vPDU.push_back(0x32); // 3030303030303032 => 00000002
        // User info
        vPDU.push_back(0xBE);
        vPDU.push_back(0x10); // Field length
        vPDU.push_back(0x04);
        vPDU.push_back(0x0E); // Length: version, block and PDU
        // DLMS Version
        vPDU.push_back(0x01);
        vPDU.push_back(0x00);
        vPDU.push_back(0x00);
        vPDU.push_back(0x00);
        vPDU.push_back(0x06); // Client DLMS version: 6
        vPDU.push_back(0x5F);
        vPDU.push_back(0x1F); // Not present in HDLC
        vPDU.push_back(0x04);
        vPDU.push_back(0x00);
        // Proposed conformance block
        vPDU.push_back(0x00); // 0x00: No option
        vPDU.push_back(0x18); 
        vPDU.push_back(0x1F); // 0x1F: get, set, selective-access, event-notification, action
        // Max PDU size
        vPDU.push_back(0x02);
        vPDU.push_back(0x00); // 2000 (HEX) => 8192 (DEC) bytes

        // Frame length
        vPDU[4] = Helper::intToHex(vPDU.size() - 5);
    }
    // DLMS request to know the capture period
    else if (requestType.compare("capturePeriod") == 0) {

        // Get Frame Tag
        vPDU.push_back(0xC0);
        // Get Frame Type, only one attribute
        vPDU.push_back(0x01); 
        // Priority
        vPDU.push_back(0xC1); 
        // Object class
        vPDU.push_back(0x00);
        vPDU.push_back(0x07);
        // OBIS Code
        vPDU.push_back(0x01); // A
        vPDU.push_back(0x00); // B
        vPDU.push_back(0x63); // C
        vPDU.push_back(0x01); // D
        vPDU.push_back(0x00); // E
        vPDU.push_back(0xFF); // F
        // Object attribute
        vPDU.push_back(0x04); 
        vPDU.push_back(0x00); // Not selective access

    }
    // DLMS request to know the date and time
    else if (requestType.compare("dateTime") == 0) {
            
        // Get Frame Tag
        vPDU.push_back(0xC0);
        // Get Frame Type, only one attribute
        vPDU.push_back(0x01); 
        // Priority
        vPDU.push_back(0xC1); 
        // Object class
        vPDU.push_back(0x00);
        vPDU.push_back(0x08); 
        // OBIS
        vPDU.push_back(0x00); // A
        vPDU.push_back(0x00); // B
        vPDU.push_back(0x01); // C
        vPDU.push_back(0x00); // D
        vPDU.push_back(0x00); // E
        vPDU.push_back(0xFF); // F
        // Object attribute
        vPDU.push_back(0x02);
        vPDU.push_back(0x00); // Not selective access
    }
    // Requests
    else if (requestType.compare("TM") == 0) {

        // Get Frame Tag
        vPDU.push_back(0xC0);
        // Get Frame Type, only one attribute
        vPDU.push_back(0x01); 
        // Priority
        vPDU.push_back(0xC1); 
        // Object class
        vPDU.push_back(0x00);
        vPDU.push_back(0x07);
        // OBIS Code
        vPDU.push_back(0x01); // A
        vPDU.push_back(0x00); // B
        vPDU.push_back(0x63); // C
        vPDU.push_back(0x01); // D
        vPDU.push_back(0x00); // E
        vPDU.push_back(0xFF); // F
        // Object attribute
        vPDU.push_back(0x02); 
        vPDU.push_back(0x01); // Selective access

        vPDU.push_back(0x01);
        vPDU.push_back(0x02);
        vPDU.push_back(0x04);
        vPDU.push_back(0x02);
        vPDU.push_back(0x04);

        vPDU.push_back(0x12);
        vPDU.push_back(0x00);
        vPDU.push_back(0x08);
        vPDU.push_back(0x09); // Data type [09 -> Octet-String]
        vPDU.push_back(0x06); // Number of Octet-String

        // OBIS CODE - CLOCK
        vPDU.push_back(0x00); // A
        vPDU.push_back(0x00); // B
        vPDU.push_back(0x01); // C
        vPDU.push_back(0x00); // D
        vPDU.push_back(0x00); // E
        vPDU.push_back(0xFF); // F

        vPDU.push_back(0x0F);
        vPDU.push_back(0x02);
        vPDU.push_back(0x12);

        vPDU.push_back(0x00);
        vPDU.push_back(0x00);

        // ####################
        // ### INITIAL DATE ###
        // ####################

        vPDU.push_back(0x09); // Data type [09 -> Octet-String]
        vPDU.push_back(0x0C); // Number of Octet-String
        
        vDate = Date::strDateToHex(initialDate);
        for (std::size_t i = 0; i < vDate.size(); i++) {

            vPDU.push_back(vDate[i]);

        }
        vDate.clear();
        
        vPDU.push_back(0x00); // Tenths of a second
        vPDU.push_back(0x80);
        vPDU.push_back(0x00);
        vPDU.push_back(0x00); // Clock state [00: Ok]

        // ##################
        // ### FINAL DATE ###
        // ##################

        vPDU.push_back(0x09); // Data type [09 -> Octet-String]
        vPDU.push_back(0x0C); // Number of Octet-String
       
        vDate = Date::strDateToHex(finalDate);
        for (std::size_t i = 0; i < vDate.size(); i++) {

            vPDU.push_back(vDate[i]);

        }
        vDate.clear();
        
        vPDU.push_back(0x00); // Tenths of a second
        vPDU.push_back(0x80);
        vPDU.push_back(0x00);
        vPDU.push_back(0x00); // Clock state [00: Ok]
        
        vPDU.push_back(0x01);
        vPDU.push_back(0x00);
        
    }
    else if (requestType.compare("TM_more") == 0) {
        
        vPDU.push_back(0xC0);
        vPDU.push_back(0x02);
        vPDU.push_back(0xC1);
        // Frame Number
        vPDU.push_back(Helper::intToHex(frameNumber) << 24);
        vPDU.push_back(Helper::intToHex(frameNumber) << 16);
        vPDU.push_back(Helper::intToHex(frameNumber) << 8);
        vPDU.push_back(Helper::intToHex(frameNumber) & 0xFF);

    }
    else if (requestType.compare("C1_1") == 0) {
        
        // Get Frame Tag
        vPDU.push_back(0xC0);
        // Get Frame Type, only one attribute
        vPDU.push_back(0x01); 
        // Priority
        vPDU.push_back(0xC1); 
        // Object class
        vPDU.push_back(0x00);
        vPDU.push_back(0x07);
        // OBIS Code
        vPDU.push_back(0x00); // A
        vPDU.push_back(0x00); // B
        vPDU.push_back(0x62); // C
        vPDU.push_back(0x01); // D
        vPDU.push_back(0x01); // E
        vPDU.push_back(0xFF); // F
        // Object attribute
        vPDU.push_back(0x02); 
        vPDU.push_back(0x01); // Selective access

        vPDU.push_back(0x01);
        vPDU.push_back(0x02);
        vPDU.push_back(0x04);
        vPDU.push_back(0x02);
        vPDU.push_back(0x04);

        vPDU.push_back(0x12);
        vPDU.push_back(0x00);
        vPDU.push_back(0x08);
        vPDU.push_back(0x09); // Data type [09 -> Octet-String]
        vPDU.push_back(0x06); // Number of Octet-String

        // OBIS CODE - CLOCK
        vPDU.push_back(0x00); // A
        vPDU.push_back(0x00); // B
        vPDU.push_back(0x01); // C
        vPDU.push_back(0x00); // D
        vPDU.push_back(0x00); // E
        vPDU.push_back(0xFF); // F

        vPDU.push_back(0x0F);
        vPDU.push_back(0x02);
        vPDU.push_back(0x12);

        vPDU.push_back(0x00);
        vPDU.push_back(0x00);

        // ####################
        // ### INITIAL DATE ###
        // ####################

        vPDU.push_back(0x09); // Data type [09 -> Octet-String]
        vPDU.push_back(0x0C); // Number of Octet-String
        
        vDate = Date::strDateToHex(initialDate);
        for (std::size_t i = 0; i < vDate.size(); i++) {

            vPDU.push_back(vDate[i]);

        }
        vDate.clear();
        
        vPDU.push_back(0x00); // Tenths of a second
        vPDU.push_back(0x80);
        vPDU.push_back(0x00);
        vPDU.push_back(0x00); // Clock state [00: Ok]

        // ##################
        // ### FINAL DATE ###
        // ##################

        vPDU.push_back(0x09); // Data type [09 -> Octet-String]
        vPDU.push_back(0x0C); // Number of Octet-String
       
        vDate = Date::strDateToHex(finalDate);
        for (std::size_t i = 0; i < vDate.size(); i++) {

            vPDU.push_back(vDate[i]);

        }
        vDate.clear();
        
        vPDU.push_back(0x00); // Tenths of a second
        vPDU.push_back(0x80);
        vPDU.push_back(0x00);
        vPDU.push_back(0x00); // Clock state [00: Ok]
        
        vPDU.push_back(0x01);
        vPDU.push_back(0x00);

    }
    else if (requestType.compare("C1_2") == 0) {
        
        // Get Frame Tag
        vPDU.push_back(0xC0);
        // Get Frame Type, only one attribute
        vPDU.push_back(0x01); 
        // Priority
        vPDU.push_back(0xC1); 
        // Object class
        vPDU.push_back(0x00);
        vPDU.push_back(0x07);
        // OBIS Code
        vPDU.push_back(0x00); // A
        vPDU.push_back(0x00); // B
        vPDU.push_back(0x62); // C
        vPDU.push_back(0x01); // D
        vPDU.push_back(0x02); // E
        vPDU.push_back(0xFF); // F
        // Object attribute
        vPDU.push_back(0x02); 
        vPDU.push_back(0x01); // Selective access

        vPDU.push_back(0x01);
        vPDU.push_back(0x02);
        vPDU.push_back(0x04);
        vPDU.push_back(0x02);
        vPDU.push_back(0x04);

        vPDU.push_back(0x12);
        vPDU.push_back(0x00);
        vPDU.push_back(0x08);
        vPDU.push_back(0x09); // Data type [09 -> Octet-String]
        vPDU.push_back(0x06); // Number of Octet-String

        // OBIS CODE - CLOCK
        vPDU.push_back(0x00); // A
        vPDU.push_back(0x00); // B
        vPDU.push_back(0x01); // C
        vPDU.push_back(0x00); // D
        vPDU.push_back(0x00); // E
        vPDU.push_back(0xFF); // F

        vPDU.push_back(0x0F);
        vPDU.push_back(0x02);
        vPDU.push_back(0x12);

        vPDU.push_back(0x00);
        vPDU.push_back(0x00);

        // ####################
        // ### INITIAL DATE ###
        // ####################

        vPDU.push_back(0x09); // Data type [09 -> Octet-String]
        vPDU.push_back(0x0C); // Number of Octet-String
        
        vDate = Date::strDateToHex(initialDate);
        for (std::size_t i = 0; i < vDate.size(); i++) {

            vPDU.push_back(vDate[i]);

        }
        vDate.clear();
        
        vPDU.push_back(0x00); // Tenths of a second
        vPDU.push_back(0x80);
        vPDU.push_back(0x00);
        vPDU.push_back(0x00); // Clock state [00: Ok]

        // ##################
        // ### FINAL DATE ###
        // ##################

        vPDU.push_back(0x09); // Data type [09 -> Octet-String]
        vPDU.push_back(0x0C); // Number of Octet-String
       
        vDate = Date::strDateToHex(finalDate);
        for (std::size_t i = 0; i < vDate.size(); i++) {

            vPDU.push_back(vDate[i]);

        }
        vDate.clear();
        
        vPDU.push_back(0x00); // Tenths of a second
        vPDU.push_back(0x80);
        vPDU.push_back(0x00);
        vPDU.push_back(0x00); // Clock state [00: Ok]
        
        vPDU.push_back(0x01);
        vPDU.push_back(0x00);

    }
    else if (requestType.compare("C1_3") == 0) {
        
        // Get Frame Tag
        vPDU.push_back(0xC0);
        // Get Frame Type, only one attribute
        vPDU.push_back(0x01); 
        // Priority
        vPDU.push_back(0xC1); 
        // Object class
        vPDU.push_back(0x00);
        vPDU.push_back(0x07);
        // OBIS Code
        vPDU.push_back(0x00); // A
        vPDU.push_back(0x00); // B
        vPDU.push_back(0x62); // C
        vPDU.push_back(0x01); // D
        vPDU.push_back(0x03); // E
        vPDU.push_back(0xFF); // F
        // Object attribute
        vPDU.push_back(0x02); 
        vPDU.push_back(0x01); // Selective access

        vPDU.push_back(0x01);
        vPDU.push_back(0x02);
        vPDU.push_back(0x04);
        vPDU.push_back(0x02);
        vPDU.push_back(0x04);

        vPDU.push_back(0x12);
        vPDU.push_back(0x00);
        vPDU.push_back(0x08);
        vPDU.push_back(0x09); // Data type [09 -> Octet-String]
        vPDU.push_back(0x06); // Number of Octet-String

        // OBIS CODE - CLOCK
        vPDU.push_back(0x00); // A
        vPDU.push_back(0x00); // B
        vPDU.push_back(0x01); // C
        vPDU.push_back(0x00); // D
        vPDU.push_back(0x00); // E
        vPDU.push_back(0xFF); // F

        vPDU.push_back(0x0F);
        vPDU.push_back(0x02);
        vPDU.push_back(0x12);

        vPDU.push_back(0x00);
        vPDU.push_back(0x00);

        // ####################
        // ### INITIAL DATE ###
        // ####################

        vPDU.push_back(0x09); // Data type [09 -> Octet-String]
        vPDU.push_back(0x0C); // Number of Octet-String
        
        vDate = Date::strDateToHex(initialDate);
        for (std::size_t i = 0; i < vDate.size(); i++) {

            vPDU.push_back(vDate[i]);

        }
        vDate.clear();
        
        vPDU.push_back(0x00); // Tenths of a second
        vPDU.push_back(0x80);
        vPDU.push_back(0x00);
        vPDU.push_back(0x00); // Clock state [00: Ok]

        // ##################
        // ### FINAL DATE ###
        // ##################

        vPDU.push_back(0x09); // Data type [09 -> Octet-String]
        vPDU.push_back(0x0C); // Number of Octet-String
       
        vDate = Date::strDateToHex(finalDate);
        for (std::size_t i = 0; i < vDate.size(); i++) {

            vPDU.push_back(vDate[i]);

        }
        vDate.clear();
        
        vPDU.push_back(0x00); // Tenths of a second
        vPDU.push_back(0x80);
        vPDU.push_back(0x00);
        vPDU.push_back(0x00); // Clock state [00: Ok]
        
        vPDU.push_back(0x01);
        vPDU.push_back(0x00);

    }
    else if (requestType.compare("C1_more") == 0) {
        
        vPDU.push_back(0xC0);
        vPDU.push_back(0x02);
        vPDU.push_back(0xC1);
        // Frame Number
        vPDU.push_back(Helper::intToHex(frameNumber) << 24);
        vPDU.push_back(Helper::intToHex(frameNumber) << 16);
        vPDU.push_back(Helper::intToHex(frameNumber) << 8);
        vPDU.push_back(Helper::intToHex(frameNumber) & 0xFF);

    }
    
    return vPDU;

}

void DLMS::setFrameNumber(unsigned int frameNumber) {

    this->frameNumber = frameNumber;

}
