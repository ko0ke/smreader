#include "../include/DLMSController.h"

template <class T>
const int DLMSController<T>::RETRIES = 5;

template <class T>
DLMSController<T>::DLMSController() {
    
    this->code = 3;
    this->retryNumber = 0;
    this->connected = false;
    this->associated = false;
    this->capturePeriod = -1;

}

template <class T>
void DLMSController<T>::read(T &port) {

    int totalRequests = 0;
    std::string strLog = "";

    File::write(port.getLogFile() + ".log", "\nCOMUNICACION DLMS");
    File::write(port.getLogFile() + ".log", "\nParametros del Modem: ");

    if (port.getDerivedClassName().compare("Socket") == 0) {
    
        File::write(port.getLogFile() + ".log", "\n\tIP: " + port.getIp() + ":" + std::to_string(port.getPort()));
    
    } else if (port.getDerivedClassName().compare("Serial") == 0) {

        File::write(port.getLogFile() + ".log", "\n\tTelefono: " + port.getPhoneNumber() + "\tCOMM: " + port.getCOMM());
    
    }

    totalRequests = port.getTotalRequests();

    // Available options
    //  -r request
    //  -o ping_modem
    //  -o ping_meter

    if (port.open() == true) {

        if ( (totalRequests > 0) || (port.getOperation().compare("ping_meter") == 0) ) {

            this->request(-1, port, "connection", "", "");
            this->request(-1, port, "association", "", "");

            if (this->associated == true) {

                this->request(-1, port, "capturePeriod", "", "");
                this->request(-1, port, "dateTime", "", "");
                // TODO: Implementar la solicitud de instantaneos y hora del contador
                
                // Requests
                for (int i = 0; i < totalRequests; i++) {

                    if (port.getCurve(i) != 3) {

                        this->request(i, port, "TM", port.getInitialDate(i), port.getFinalDate(i));

                    } else if (port.getCurve(i) == 3) {

                        std::vector<std::string> vAuxRequest;

                        if (port.getCurveType(i).compare("T") == 0) {

                            vAuxRequest.push_back("C1_1");
                            vAuxRequest.push_back("C1_2");
                            vAuxRequest.push_back("C1_3");

                        } else if (port.getCurveType(i).compare("1") == 0) {

                            vAuxRequest.push_back("C1_1");

                        } else if (port.getCurveType(i).compare("2") == 0) {

                            vAuxRequest.push_back("C1_2");

                        } else if (port.getCurveType(i).compare("3") == 0) {

                            vAuxRequest.push_back("C1_3");

                        } 

                        for (std::size_t k = 0; k < vAuxRequest.size(); k++) {

                            this->request(i, port, vAuxRequest[k], port.getInitialDate(i), port.getFinalDate(i));

                        }

                    }

                }

            }
            
            this->request(-1, port, "disconnection", "", "");

        }

        port.close();

    }

    // TODO: 
    // * Añadir el resultado para las operaciones (-o)

    strResults.append("[");

    for (int i = 0; i < totalRequests; i++) {

        strResults.append("{");
        strResults.append("\"codigo\": \"" + std::to_string(port.getCode(i)) + "\", ");
        strResults.append("\"mensaje\": \"" + port.getMessage(i) + "\", ");
        strResults.append("\"fechaIniMinutaje\": \"" + port.getStartCall(i) + "\", ");
        strResults.append("\"fechaFinMinutaje\": \"" + port.getEndCall(i) + "\", ");

        std::string fileNameCSV = "";
        std::string cType = "_TM_incremental_";
        if (port.getTotalDownloadedDates(i) > 0) {
        
            if (port.getCurve(i) == 3) {

                cType = "_C1_" + port.getCurveType(i);

            }
            
            fileNameCSV = port.getLogFile() + to_string(i) + cType + this->strDateToStrFileName(port.getInitialDate(i)) + this->strDateToStrFileName(port.getFinalDate(i));

        }
        strResults.append("\"fichero\": \"" + fileNameCSV + "\", ");
        strResults.append("\"registros\": \"" + std::to_string(port.getTotalDownloadedDates(i)) + "\", ");

        strResults.append("\"fechaServidor\": \"" + port.getDateTimeServer() + "\", ");
        strResults.append("\"fechaContador\": \"" + port.getDateTimeMeter() + "\", ");

        if (this->capturePeriod == 15) {

            strResults.append("\"periodoCaptura\": \"TM2\", ");

        } else if (this->capturePeriod == 60) {

            strResults.append("\"periodoCaptura\": \"TM1\", ");

        } else if (this->capturePeriod == 0) {

            strResults.append("\"periodoCaptura\": \"C1\", ");
        
        } else {

            strResults.append("\"periodoCaptura\": \"-1\", ");

        }

        strResults.append("\"solicitud\": \"" + std::to_string(i) + "\", ");

        if (port.getCurve(i) == 1) {

            strResults.append("\"curva\": \"TM1\", ");

        } else if (port.getCurve(i) == 2) {

            strResults.append("\"curva\": \"TM2\", ");

        } else if (port.getCurve(i) == 3) {

            strResults.append("\"curva\": \"C1\", ");
        
        }

        if (port.getCurveType(i).compare("inc") == 0) {

            strResults.append("\"tipoCurva\": \"inc\", ");

        } else if (port.getCurveType(i).compare("abs") == 0) {

            strResults.append("\"tipoCurva\": \"abs\", ");

        } else if (port.getCurveType(i).compare("1") == 0) {

            strResults.append("\"tipoCurva\": \"C1\", ");
        
        } else if (port.getCurveType(i).compare("2") == 0) {

            strResults.append("\"tipoCurva\": \"C2\", ");
        
        } else if (port.getCurveType(i).compare("3") == 0) {

            strResults.append("\"tipoCurva\": \"C3\", ");
        
        } else if (port.getCurveType(i).compare("T") == 0) {

            strResults.append("\"tipoCurva\": \"CT\", ");
        
        }
        
        strResults.append("\"fechaInicial\": \"" + port.getInitialDate(i) + "\", ");
        strResults.append("\"fechaFinal\": \"" + port.getFinalDate(i) + "\"");

        strResults.append("}");

        if ( i < (totalRequests - 1) ) {

            strResults.append(",");

        }

    }

    strResults.append("]");

}

template <class T>
void DLMSController<T>::request(int idRequest, T &port, std::string frameType, std::string initalDate, std::string finalDate) {

    std::string strLog = "";
    // ### REQUEST CODES ###
    //
    //  1. Frame OK. Keep reading.
    //  2. Nothing received or frame received, but is not the frame that we were waiting for. Try it again.
    //  3. Keep requesting.
    //  4. Stop requesting.
    //  5. Stop communication.
    //

    // TODO: Result codes
    // ### RESULT CODES ###
    //
    // 0 = Sin Errores, Todo Ok.
    // 
    // 1 = Modem llamante no responde.
    // 2 = El modem remoto no responde (GSM).
    // 3 = No se ha podido abrir el socket.
    // 4 = El modem remoto no responde (IP).
    //
    // 5 = No se ha podido enlazar con el contador.
    // 6 = Asociacion con el contador rechazada.
    // 7 = Corte de comunicacion tras solicitar el Periodo de Captura.
    // 8 = Corte de comunicacion tras solicitar la Asociacion con el contador.
    // 9 = Corte de comunicacion descargando datos [x registros descargados].
    // 10 = El contador no ha devuelto o no dispone de los datos.
    
    //    

    if ( ( (this->code != 5) || (this->connected == true) && (frameType.compare("disconnection") == 0) ) ) {

        if ( (this->connected == true) && (frameType.compare("disconnection") == 0) ) {

            this->retryNumber = 0;

        }

        if (frameType.compare("connection") == 0) {

            File::write(port.getLogFile() + ".log", "\n\n\t###########################");
            File::write(port.getLogFile() + ".log", "\n\t#####  Conexion HDLC  #####");
            File::write(port.getLogFile() + ".log", "\n\t###########################");

        }
        else if (frameType.compare("association") == 0) {

            File::write(port.getLogFile() + ".log", "\n\n\t##############################");
            File::write(port.getLogFile() + ".log", "\n\t#####  Asociacion DLMS  ######");
            File::write(port.getLogFile() + ".log", "\n\t##############################");

        }
        else if (frameType.compare("capturePeriod") == 0) {

            File::write(port.getLogFile() + ".log", "\n\n\t#################################");
            File::write(port.getLogFile() + ".log", "\n\t#####  Periodo de Captura  ######");
            File::write(port.getLogFile() + ".log", "\n\t#################################");

        }
        else if (frameType.compare("dateTime") == 0) {

            File::write(port.getLogFile() + ".log", "\n\n\t########################################");
            File::write(port.getLogFile() + ".log", "\n\t#####  Fecha y hora del Contador  ######");
            File::write(port.getLogFile() + ".log", "\n\t########################################");

        }
        else if (frameType.compare("TM") == 0) {
            
            File::write(port.getLogFile() + ".log", "\n\n\t##############################################################");
            File::write(port.getLogFile() + ".log", "\n\t#####  TM ENTRE " + initalDate + " Y " + finalDate + "  ######");
            File::write(port.getLogFile() + ".log", "\n\t##############################################################");

        }
        else if (frameType.compare("C1_1") == 0) {

            File::write(port.getLogFile() + ".log", "\n\n\t################################################################");
            File::write(port.getLogFile() + ".log", "\n\t#####  C1_1 ENTRE " + initalDate + " Y " + finalDate + "  ######");
            File::write(port.getLogFile() + ".log", "\n\t################################################################");

        }
        else if (frameType.compare("C1_2") == 0) {

            File::write(port.getLogFile() + ".log", "\n\n\t################################################################");
            File::write(port.getLogFile() + ".log", "\n\t#####  C1_2 ENTRE " + initalDate + " Y " + finalDate + "  ######");
            File::write(port.getLogFile() + ".log", "\n\t################################################################");

        }
        else if (frameType.compare("C1_3") == 0) {

            File::write(port.getLogFile() + ".log", "\n\n\t################################################################");
            File::write(port.getLogFile() + ".log", "\n\t#####  C1_3 ENTRE " + initalDate + " Y " + finalDate + "  ######");
            File::write(port.getLogFile() + ".log", "\n\t################################################################");

        }
        else if (frameType.compare("disconnection") == 0) {

            File::write(port.getLogFile() + ".log", "\n\n\t############################");
            File::write(port.getLogFile() + ".log", "\n\t##### Desconexion HDLC #####");
            File::write(port.getLogFile() + ".log", "\n\t############################");

        }

        do {

            File::write(port.getLogFile() + ".log", "\n\n\n ##########################");
            File::write(port.getLogFile() + ".log", "\n ##### [Intento: " +std::to_string(this->retryNumber + 1) + "/" +std::to_string(RETRIES) + "] ##### ");
            File::write(port.getLogFile() + ".log", "\n ##########################");
            File::write(port.getLogFile() + ".log", "\n");
            
            port.sendFrame(hdlc.frame(frameType, initalDate, finalDate));
            this->code = this->analyze(idRequest, port, this->clean(port.receiveFrame()), frameType);

            if ( (frameType.compare("TM") == 0) && (this->code == 3) ) {

                frameType = "TM_more";

            }

            std::size_t found = frameType.find("C1_");
            if ( (found != std::string::npos) && (this->code == 3) ) {

                frameType = "C1_more";

            }

            // Nothing received or frame received, but is not the frame that we were waiting for. Try it again.
            if (this->code == 2) {

                this->retryNumber = this->retryNumber + 1;

                if (this->retryNumber >= RETRIES) {

                    this->code = 5;

                    if (frameType.compare("connection") == 0) {

                        for (std::size_t i = 0; i < port.getTotalRequests(); i++) {
                    
                            port.setCode(i, 5);
                            port.setMessage(i, "No se ha podido enlazar con el contador.");
                        
                        }
                        File::write(port.getLogFile() + ".log", "\n\n +++++ [LOG] [No se ha podido enlazar con el contador.] +++++");

                    } else if (frameType.compare("capturePeriod") == 0) {

                        // 7 = Corte de comunicacion tras solicitar el Periodo de Captura.
                        for (std::size_t i = 0; i < port.getTotalRequests(); i++) {
                    
                            port.setCode(i, 7);
                            port.setMessage(i, "Corte de comunicacion tras solicitar el Periodo de Captura.");
                        
                        }
                        File::write(port.getLogFile() + ".log", "\n\n +++++ [LOG] [Corte de comunicacion tras solicitar el Periodo de Captura.] +++++");

                    } else if (frameType.compare("association") == 0) {

                        // 8 = Corte de comunicacion tras solicitar la Asociacion con el contador.
                        for (std::size_t i = 0; i < port.getTotalRequests(); i++) {
                    
                            port.setCode(i, 8);
                            port.setMessage(i, "Corte de comunicacion tras solicitar la Asociacion con el contador.");
                        
                        }
                        File::write(port.getLogFile() + ".log", "\n\n +++++ [LOG] [Corte de comunicacion tras solicitar la Asociacion con el contador.] +++++");
                        
                    } else if (frameType.compare("dateTime") == 0) {

                        // 7 = Corte de comunicacion tras solicitar la fecha y hora del contador..
                        for (std::size_t i = 0; i < port.getTotalRequests(); i++) {
                    
                            port.setCode(i, 7);
                            port.setMessage(i, "Corte de comunicacion tras solicitar la fecha y hora del contador.");
                        
                        }
                        File::write(port.getLogFile() + ".log", "\n\n +++++ [LOG] [Corte de comunicacion tras solicitar la fecha y hora del contador.] +++++");

                    } else if ( (frameType.compare("TM") == 0) || (frameType.compare("TM_more") == 0) || (found != std::string::npos) ) {

                        // 9 = Corte de comunicacion descargando datos [x registros descargados].
                        for (std::size_t i = 0; i < port.getTotalRequests(); i++) {
                        
                            port.setCode(i, 9);
                            port.setMessage(i, "Corte de comunicacion descargando datos [" + to_string(port.getTotalDownloadedDates(i)) + " registros descargados]");
                        
                        }

                    }

                }

            } else {

                this->retryNumber = 0;

            }

        } while ( (this->retryNumber < RETRIES) && ( (this->code == 2) || (this->code == 3) ) );

    }

}

template <class T>
std::vector<unsigned char> DLMSController<T>::clean(std::vector<unsigned char> vFrame) {

    bool duplicated = true;
    std::vector<unsigned char> vFrameAux;
    std::vector<unsigned char> vFinalFrame;
    int frameLength = 0;

    // Erase the unknown bytes
    for (std::size_t i = 0; i < vFrame.size(); i++) {

        // If it is a well formed frame
        if ( (vFrame[i] == 0x7E) && ( (vFrame.size() - 1) >= (vFrame[i + 2] + 1) ) && (vFrame[vFrame[i + 2] + 1] == 0x7E) ) {

            i = i + (int)vFrame[i + 2] + 1;

        } else {

            vFrame.erase(vFrame.begin() + i);
            i = i - 1;

        }

    }

    // Erase the duplicated frames
    for (std::size_t i = 0; i < vFrame.size(); i++) {

        // Copy the frame that we are going to analyze
        std::copy(vFrame.begin(), (vFrame.begin() + (vFrame[2] + 2)), back_inserter(vFrameAux));
        frameLength = (vFrame[i + 2] + 1);

        // Delete the frame from the received frame
        vFrame.erase(vFrame.begin(), (vFrame.begin() + (vFrame[2] + 2)));

        // Check duplicate frames
        while (duplicated == true) {

            auto it = std::search(vFrame.begin(), vFrame.end(), vFrameAux.begin(), vFrameAux.end());
            if (it != vFrame.end()) {

                // Duplicated frame
                duplicated = true;
                auto begin = it - vFrame.begin();
                vFrame.erase(vFrame.begin() + begin, ((vFrame.begin() + begin) + frameLength));

            } else {

                // Duplicated frame not found
                duplicated = false;

                std::copy(vFrame.begin(), (vFrame.begin() + (vFrame[2] + 2)), back_inserter(vFinalFrame));

            }

        }

    }

    vFrameAux.clear();
    vFrame.clear();

    return vFinalFrame;

}

template <class T>
int DLMSController<T>::analyze(int idRequest, T &port, std::vector<unsigned char> vFrame, std::string requestType) {

    int code = 2;
    std::vector<unsigned char> vFrameAux;
    std::vector<unsigned char> vFind;
    std::string strLog = "";

    // ### CODES ###
    //
    //  1. Frame OK. Keep reading.
    //  2. Nothing received or frame received, but is not the frame that we were waiting for. Try it again.
    //  3. Keep requesting.
    //  4. Stop requesting.
    //  5. Stop communication.
    //

    // Check if nothing is received
    if (vFrame.empty() == true) {

        code = 2;
        port.updateTimeout(500);

    } else {

        port.resetTimeout();
        
        while (vFrame.empty() == false) {

            // Copy the frame that we are going to analyze
            std::copy(vFrame.begin(), (vFrame.begin() + (vFrame[2] + 2)), back_inserter(vFrameAux));

            // Delete the frame from the received frame
            vFrame.erase(vFrame.begin(), (vFrame.begin() + (vFrame[2] + 2))); 

            std::size_t found = requestType.find("C1_");       
            
            // HDLC Connection
            if (requestType.compare("connection") == 0) {
                
                vFind.push_back(0x21);
                vFind.push_back(0x73);

                // +++ CIRCUTOR / SAGEMCOM +++
                // 7E A0 1F 03    02    {21 73} 73 42 {81 80 12 05 01 80 06 01 80 07 04 00 00 00 01 08 04 00 00 00 01} 53 3B 7E
                // {73} -> Response UA
                // {81 80 12 05 01 80 06 01 80 07 04 00 00 00 01 08 04 00 00 00 01} -> Negotiation block

                // +++ ZIV +++
                // 7e a0 17 03 00 02 00 {21 73} 4c c2 {81 80 08 05 02 00 ff 06 02 00 ff} 87 71 7e
                // {73} -> Response UA
                // {81 80 08 05 02 00 ff 06 02 00 ff} -> Negotiation block
                
                auto it = std::search(vFrameAux.begin(), vFrameAux.end(), vFind.begin(), vFind.end());
                if (it != vFrameAux.end()) {
                    
                    code = 1;
                    this->connected = true;
                    File::write(port.getLogFile() + ".log", "\n\n +++++ [LOG] [HDLC conectado] +++++");

                }

                vFind.clear();

            }
            // DLMS AARE
            else if (requestType.compare("association") == 0) {

                vFind.push_back(0xE6);
                vFind.push_back(0xE7);
                vFind.push_back(0x00);

                // +++ CIRCUTOR / SAGEMCOM +++
                // 7E A0 38 03    02    21 30 A1 62 {{E6 E7 00} {61} 29 A1 09 06 07 60 85 74 05 08 01 01 {A2 03 02 01} {00} A3 05 A1 03 02 01 00                                     BE 10 04 0E 08 00 06 5F 1F 04 00 00 18 1F 00 6E 00 07} 28 CF 7E
                // {E6 E7 00} -> DLMS Response 
                // {61} -> DLMS AARE
                // {A2 03 02 01} -> Association result
                // {00} -> Accepted | {01} -> Refused
                
                // +++ ZIV +++
                // 7e a0 46 03 00 02 00 21 30 c9 5f {{e6 e7 00} {61} 35 a1 09 06 07 60 85 74 05 08 01 01 {a2 03 02 01} {00} a3 05 a1 03 02 01 00 a4 0a 04 08 30 30 30 38 33 39 38 31 be 10 04 0e 08 00 06 5f 1f 04 00 00 18 1d 00 fc 00 07} 69 b5 7e
                // {E6 E7 00} -> DLMS Response 
                // {61} -> DLMS AARE
                // {A2 03 02 01} -> Association result
                // {00} -> Accepted | {01} -> Refused

                // Check if the received frame is a DLMS Response
                auto it = std::search(vFrameAux.begin(), vFrameAux.end(), vFind.begin(), vFind.end());
                if (it != vFrameAux.end()) {

                    // Store the control field
                    this->hdlc.setLastControlField(vFrameAux[((it - vFrameAux.begin()) - 3)]);
                    
                    // DLMS AARE
                    if (vFrameAux[((it - vFrameAux.begin()) + vFind.size())] == 0x61) {

                        vFind.clear();

                        vFind.push_back(0xA2);
                        vFind.push_back(0x03);
                        vFind.push_back(0x02);
                        vFind.push_back(0x01);

                        // Check the association result
                        auto it = std::search(vFrameAux.begin(), vFrameAux.end(), vFind.begin(), vFind.end());
                        if (it != vFrameAux.end()) {

                            if (vFrameAux[((it - vFrameAux.begin()) + vFind.size())] == 0x00) {

                                this->associated = true;
                                File::write(port.getLogFile() + ".log", "\n\n +++++ [LOG] [Asociacion DLMS aceptada] +++++");

                            } else {

                                 for (std::size_t i = 0; i < port.getTotalRequests(); i++) {
                    
                                    port.setCode(i, 6);
                                    port.setMessage(i, "Asociacion con el contador rechazada.");
                                
                                }
                                File::write(port.getLogFile() + ".log", "\n\n +++++ [LOG] [Asociacion con el contador rechazada.] +++++");

                            }
                            
                            code = 1;

                        }

                    }

                    vFind.clear();

                }

            }
            // Capture period
            else if (requestType.compare("capturePeriod") == 0) {
                
                unsigned long int capturePeriod = 0;
                vFind.push_back(0xE6);
                vFind.push_back(0xE7);
                vFind.push_back(0x00);

                // CIRCUTOR / SAGEMCOM
                // 7E A0 16 03    02    21 52 9C 23 {{E6 E7 00} {C4} 01 C1 00 06 {00 00 0E 10}} EC 92 7E
                // {E6 E7 00} -> DLMS Response 
                // {C4} -> GET Response TAG

                // ZIV
                // 7E A0 16 03 00 02 00 21 52 9C 23 {{E6 E7 00} {C4} 01 C1 00 06 {00 00 0E 10}} EC 92 7E

                // Check if the received frame is a DLMS Response
                auto it = std::search(vFrameAux.begin(), vFrameAux.end(), vFind.begin(), vFind.end());
                if (it != vFrameAux.end()) {

                    // Store the control field
                    this->hdlc.setLastControlField(vFrameAux[((it - vFrameAux.begin()) - 3)]);

                    // GET Response TAG
                    if (vFrameAux[((it - vFrameAux.begin()) + vFind.size())] == 0xC4) {

                        vFind.clear();

                        vFind.push_back(0x00);
                        vFind.push_back(0x06); // Data type. 0x06 -> Unsigned Long Int (32 bytes)

                        auto it = std::search(vFrameAux.begin(), vFrameAux.end(), vFind.begin(), vFind.end());
                        if (it != vFrameAux.end()) {

                            int i = ((it - vFrameAux.begin()) + vFind.size());

                            capturePeriod = vFrameAux[i] << 24 | vFrameAux[i + 1] << 16 | vFrameAux[i + 2] << 8 | (vFrameAux[i + 3] & 0xFF);

                            // Convert miliseconds to minutes
                            this->capturePeriod = capturePeriod / 60;

                            File::write(port.getLogFile() + ".log", "\n\n +++++ [LOG] [Periodo de Captura: " +std::to_string(this->capturePeriod) + " minutos] +++++ ");

                            code = 1;

                        }

                        vFind.clear();

                    }
                
                }
            
            }
            // DateTime
            else if (requestType.compare("dateTime") == 0) {

                std::vector<unsigned char> vDate;
                
                // +++ CIRCUTOR / SAGEMCOM +++
                // 7E A0 1F 03    02    21 74 CC 36 {E6 E7 00} C4 01 C1 00 09 0C 07 E3 07 1A 05 15 28 23 FF 80 00 80 B1 D8 7E 
                // {E6 E7 00} -> DLMS Response

                // +++ ZIV +++
                // 7E A0 21 03 00 02 00 21 74 74 38 {E6 E7 00} C4 01 C4 00 09 0C 07 E3 07 1A 05 15 21 10 00 80 00 80 7F 57 7E
                // {E6 E7 00} -> DLMS Response

                vFind.push_back(0xE6);
                vFind.push_back(0xE7);
                vFind.push_back(0x00);
                // Check if the received frame is a DLMS Response
                auto it = std::search(vFrameAux.begin(), vFrameAux.end(), vFind.begin(), vFind.end());
                if (it != vFrameAux.end()) {

                    // Store the control field
                    this->hdlc.setLastControlField(vFrameAux[((it - vFrameAux.begin()) - 3)]);

                    vFrameAux.erase(vFrameAux.begin(), it + vFind.size());
                    vFind.clear();

                    // GET Response TAG
                    vFind.push_back(0xC4);
                    auto it = std::search(vFrameAux.begin(), vFrameAux.end(), vFind.begin(), vFind.end());
                    if (it != vFrameAux.end()) {

                        vFrameAux.erase(vFrameAux.begin(), it + vFind.size() + 3);
                        vFind.clear();

                        vFind.push_back(0x09);
                        vFind.push_back(0x0C);

                        auto it = std::search(vFrameAux.begin(), vFrameAux.end(), vFind.begin(), vFind.end());
                        if (it != vFrameAux.end()) {

                            vFind.clear();
                            vFrameAux.erase(vFrameAux.end() - 3, vFrameAux.end());

                            File::write(port.getLogFile() + ".log", "\nframeToString: " + Helper::frameToString(vFrameAux));
                            // Date
                            File::write(port.getLogFile() + ".log", "\nMeter datetime: " + Date::hexDateToStr(vFrameAux));
                            File::write(port.getLogFile() + ".log", "\nServer datetime: " + Date::now());
                            port.setDateTimeMeter(Date::hexDateToStr(vFrameAux));
                            port.setDateTimeServer(Date::now());
                            code = 1;

                        }

                    }

                }

            }
            // TM
            else if ( (requestType.compare("TM") == 0) || (requestType.compare("TM_more") == 0) ) {

                // TODO: 
                // * Revisar la curva con contadores ZIV
                // * Control de huecos

                vFind.push_back(0xE6);
                vFind.push_back(0xE7);
                vFind.push_back(0x00);

                // CIRCUTOR / SAGEMCOM
                // 7E A0 79 03 02 21 74 E7 AC 
                // {E6 E7 00} -> DLMS Response
                // {C4} -> GET Response TAG
                // {02} -> List (More than one frame) 
                // C1 
                // {00} -> 0x00 -> More frames. 0x01 -> No more frames 
                // {00 00 00 01} -> Frame number
                // 00 62 
                // {01} -> Data type. 0x01 -> Array 
                // {07} -> Total elements in the array 
                // {02} -> Data type. 0x02 -> Structure
                // {08} -> Total elements in the structure  
                // {09 0C 07 E3 07 0F 01 09 00 00 00 80 00 80} {11 02} {06 {00 00 00 00}} {06 {00 00 00 00}} {06 {00 00 00 00}} {06 {00 00 00 00}} {06 {00 00 00 00}} {06 00 00 00 00} 
                // {02} -> Data type. 0x02 -> Structure
                // {08} -> Total elements in the structure
                // {09 0C 07 E3 07 0F 01 09 0F 00 00 80 00 80} {11 02} {06 00 00 00 00} {06 00 00 00 00} {06 00 00 00 00} {06 00 00 00 00} {06 00 00 00 00} {06 00 00 00 00} 
                // FD 76 7E

                // Check if the received frame is a DLMS Response
                auto it = std::search(vFrameAux.begin(), vFrameAux.end(), vFind.begin(), vFind.end());
                if (it != vFrameAux.end()) {

                    // Store the control field
                    this->hdlc.setLastControlField(vFrameAux[((it - vFrameAux.begin()) - 3)]);

                    vFrameAux.erase(vFrameAux.begin(), it + vFind.size());
                    vFind.clear();

                    // GET Response TAG
                    vFind.push_back(0xC4);
                    auto it = std::search(vFrameAux.begin(), vFrameAux.end(), vFind.begin(), vFind.end());
                    if (it != vFrameAux.end()) {

                        vFrameAux.erase(vFrameAux.begin(), it + vFind.size());
                        vFind.clear();

                        // Check if the data is included in one frame
                        if (vFrameAux[0] == 0x01) {
                            
                            File::write(port.getLogFile() + ".log", "\nUNA TRAMA");
                            vFrameAux.erase(vFrameAux.begin(), vFrameAux.begin() + 5);
                            vFrameAux.erase(vFrameAux.end() - 3, vFrameAux.end());

                            for (std::size_t i = 0; i < vFrameAux.size(); i++) {

                                vReceivedData.push_back(vFrameAux[i]);

                            }
                            
                            if (this->vReceivedData.empty() == false) {

                                this->extractCurve(idRequest, port);

                            } else {

                                port.setCode(idRequest, 10);
                                port.setMessage(idRequest, "El contador no ha devuelto o no dispone de los datos solicitados.");
                                
                                File::write(port.getLogFile() + ".log", "\n\n +++++ [LOG] [El contador no ha devuelto o no dispone de los datos solicitados.] +++++");

                            }

                            code = 1;
                        
                        } 
                        // Check if the data is included in more than one frame
                        else if (vFrameAux[0] == 0x02) {

                            vFrameAux.erase(vFrameAux.begin(), vFrameAux.begin() + 2);
                            
                            // Check if is the last frame or not 
                            // 0x00 -> More frames
                            if (vFrameAux[0] == 0x00) {

                                File::write(port.getLogFile() + ".log", "\nHAY MAS TRAMAS");
                                code = 3;

                            }
                            //  0x01 -> No more frames
                            else if (vFrameAux[0] == 0x01) {

                                File::write(port.getLogFile() + ".log", "\nNO HAY MAS TRAMAS");
                                code = 1;

                            }

                            vFrameAux.erase(vFrameAux.begin(), vFrameAux.begin() + 1);

                            unsigned int nFrame = vFrameAux[0] << 24 | vFrameAux[1] << 16 | vFrameAux[2] << 8 | (vFrameAux[3] & 0xFF);
                            this->hdlc.setFrameNumber(nFrame);
                            File::write(port.getLogFile() + ".log", "\nTRAMA NUMERO: " + std::to_string(nFrame)); 
                            
                            if (nFrame == 1) {

                                vFrameAux.erase(vFrameAux.begin(), vFrameAux.begin() + 8);

                            } else {

                                vFrameAux.erase(vFrameAux.begin(), vFrameAux.begin() + 6);

                            }
                            
                            vFrameAux.erase(vFrameAux.end() - 3, vFrameAux.end());

                            // Some meters have a flag that says the size of the data
                            if (vFrameAux[0] == (vFrameAux.size() - 1)) {

                                vFrameAux.erase(vFrameAux.begin(), vFrameAux.begin() + 1);

                            }

                            for (std::size_t i = 0; i < vFrameAux.size(); i++) {

                                vReceivedData.push_back(vFrameAux[i]);

                            }

                            this->extractCurve(idRequest, port);
                            
                            if (Date::compare(Date::nextDateToDownload(port.getLastDownloadedDate(idRequest), this->capturePeriod), port.getFinalDate(idRequest)) != 2) {

                                code = 1;

                            }

                        }

                    }

                }

                vFind.clear();

            }
            // C1
            else if ( (found != std::string::npos) || (requestType.compare("C1_more") == 0) ) {

                vFind.push_back(0xE6);
                vFind.push_back(0xE7);
                vFind.push_back(0x00);

                // CIRCUTOR / SAGEMCOM
                // 7E A0 79 03 02 21 74 E7 AC 
                // {E6 E7 00} -> DLMS Response
                // {C4} -> GET Response TAG
                // {02} -> List (More than one frame) 
                // C1 
                // {00} -> 0x00 -> More frames. 0x01 -> No more frames 
                // {00 00 00 01} -> Frame number
                // 00 62 
                // {01} -> Data type. 0x01 -> Array 
                // {07} -> Total elements in the array 
                // {02} -> Data type. 0x02 -> Structure
                // {64} -> Total elements in the structure  
                // {09 0C {07 E3 06 04 02 0D 23 0C FF 80 00 80} 09 0C {07 E3 07 0B 04 0C 1C 05 FF 80 00 80} 06 {00 0F 0B 13} 06 {00 03 D0 72} 06 {00 06 DF B0} 06 {00 02 0E AC} 06 {00 00 65 11} 06 {00 01 21 6B} 06 {00 00 C5 C9} 06 {00 00 00 57} 06 {00 00 00 00} 06 {00 00 00 00} 06 {00 00 00 00} 06 {00 00 00 00} 06 {00 00 00 00} 06} 
                // 75 B6 7E

                // Check if the received frame is a DLMS Response
                auto it = std::search(vFrameAux.begin(), vFrameAux.end(), vFind.begin(), vFind.end());
                if (it != vFrameAux.end()) {

                    // Store the control field
                    this->hdlc.setLastControlField(vFrameAux[((it - vFrameAux.begin()) - 3)]);

                    vFrameAux.erase(vFrameAux.begin(), it + vFind.size());
                    vFind.clear();

                    // GET Response TAG
                    vFind.push_back(0xC4);
                    auto it = std::search(vFrameAux.begin(), vFrameAux.end(), vFind.begin(), vFind.end());
                    if (it != vFrameAux.end()) {

                        vFrameAux.erase(vFrameAux.begin(), it + vFind.size());
                        vFind.clear();

                        // Check if the data is included in one frame
                        if (vFrameAux[0] == 0x01) {
                            
                            File::write(port.getLogFile() + ".log", "\nUNA TRAMA");

                            vFrameAux.erase(vFrameAux.begin(), vFrameAux.begin() + 5);
                            vFrameAux.erase(vFrameAux.end() - 3, vFrameAux.end());

                            for (std::size_t i = 0; i < vFrameAux.size(); i++) {

                                vReceivedData.push_back(vFrameAux[i]);

                            }

                            if (this->vReceivedData.empty() == false) {

                                this->extractClosure(idRequest, port);

                            } else {

                                port.setCode(idRequest, 10);
                                port.setMessage(idRequest, "El contador no ha devuelto o no dispone de los datos solicitados.");

                                File::write(port.getLogFile() + ".log", "\n\n +++++ [LOG] [El contador no ha devuelto o no dispone de los datos solicitados] +++++");

                            }

                            code = 1;
                        
                        } 
                        // Check if the data is included in more than one frame
                        else if (vFrameAux[0] == 0x02) {

                            vFrameAux.erase(vFrameAux.begin(), vFrameAux.begin() + 2);
                            
                            // Check if is the last frame or not 
                            // 0x00 -> More frames
                            if (vFrameAux[0] == 0x00) {

                                File::write(port.getLogFile() + ".log", "\nHAY MAS TRAMAS");
                                code = 3;

                            }
                            // 0x01 -> No more frames
                            else if (vFrameAux[0] == 0x01) {

                                File::write(port.getLogFile() + ".log", "\nNO HAY MAS TRAMAS");

                                code = 1;

                            }

                            vFrameAux.erase(vFrameAux.begin(), vFrameAux.begin() + 1);

                            unsigned int nFrame = vFrameAux[0] << 24 | vFrameAux[1] << 16 | vFrameAux[2] << 8 | (vFrameAux[3] & 0xFF);
                            this->hdlc.setFrameNumber(nFrame);
                            File::write(port.getLogFile() + ".log", "\nTRAMA NUMERO: " + std::to_string(nFrame)); 
                            
                            if (nFrame == 1) {

                                vFrameAux.erase(vFrameAux.begin(), vFrameAux.begin() + 11);

                            } else {

                                vFrameAux.erase(vFrameAux.begin(), vFrameAux.begin() + 6);

                            }
                            
                            vFrameAux.erase(vFrameAux.end() - 3, vFrameAux.end());

                            // Some meters have a flag that says the size of the data
                            File::write(port.getLogFile() + ".log", "\nvFrameAux[0] : " + to_string((int)vFrameAux[0])); 
                            File::write(port.getLogFile() + ".log", "\nvFrameAux.size() : " + to_string((int)vFrameAux.size())); 
                            File::write(port.getLogFile() + ".log", "\nvFrameAux: " + Helper::frameToString(vFrameAux));
                            
                            if (vFrameAux[0] == vFrameAux.size() - 1) {

                                File::write(port.getLogFile() + ".log", "\nBORRADO"); 
                                vFrameAux.erase(vFrameAux.begin(), vFrameAux.begin() + 1);

                            }    

                            for (std::size_t i = 0; i < vFrameAux.size(); i++) {

                                vReceivedData.push_back(vFrameAux[i]);

                            }

                            if (code == 1) {

                                this->extractClosure(idRequest, port);

                            }

                        }

                    }

                }

                vFind.clear();

            }
            // HDLC Desconnection
            else if (requestType.compare("disconnection") == 0) {
                
                vFind.push_back(0x21);
                vFind.push_back(0x73);

                // +++ CIRCUTOR / SAGEMCOM +++
                // 7E A0 08 03    02    {21 73} EF C6 7E
                // {73} -> Response UA

                // +++ ZIV +++
                // 7e a0 0a 03 00 02 00 {21 73} ca 59 7e
                // {73} -> Response UA

                auto it = std::search(vFrameAux.begin(), vFrameAux.end(), vFind.begin(), vFind.end());
                if (it != vFrameAux.end()) {
                    
                    code = 1;

                }

                vFind.clear();

            }

            vFrameAux.clear();
            
        }

    }
    
    std::string strMessage = "";
    std::string strResult = "";
    if (code == 1) strMessage = "##### [Codigo: 1] [Message: Trama OK. Sigue leyendo.] #####";
    if (code == 2) strMessage = "##### [Codigo: 2] [Message: No se ha recibido nada o se ha recibido una trama, pero no era lo que se esperaba. Solicitar otra vez la ultima trama.] #####";
    if (code == 3) strMessage = "##### [Codigo: 3] [Message: Sigue solicitando.] #####";
    if (code == 4) strMessage = "##### [Codigo: 4] [Message: Parar de solicitar.] #####";
    if (code == 5) strMessage = "##### [Codigo: 5] [Message: Terminar la comunicacion.] #####";

    strResult.append("\n\n ");
    for (int i = 0; i < strMessage.length(); i++) strResult.append("#");
    strResult.append("\n ");
    strResult.append(strMessage);
    strResult.append("\n ");
    for (int i = 0; i < strMessage.length(); i++) strResult.append("#");
    strResult.append("\n");
    
    File::write(port.getLogFile() + ".log", strResult);

    return code;

}

template <class T>
void DLMSController<T>::extractCurve(int idRequest, T &port) {
    
    File::write(port.getLogFile() + ".log", "\nExtraer curva: " + Helper::frameToString(this->vReceivedData));
    
    //                 |Date-------------------------------|      |Bc|      |Ai---------|      |Ae---------|      |R1---------|      |R2---------|      |R3---------|      |R4---------|
    // {02 08} {09 0C} {07 E3 07 10 02 0A 0F 00 00 80 00 80} {11} {02} {06} {00 00 00 00} {06} {00 00 00 00} {06} {00 00 00 00} {06} {00 00 00 00} {06} {00 00 00 00} {06} {00 00 00 00} 
    // {02 08} {09 0C} {07 E3 07 10 02 0A 1E 00 00 80 00 80} {11} {02} {06} {00 00 00 00} {06} {00 00 00 00} {06} {00 00 00 00} {06} {00 00 00 00} {06} {00 00 00 00} {06} {00 00 00 00} 
    // {02 08} {09 0C} {07 E3 07 10 02 0A 2D 00 00 80 00 80} {11} {02} {06} {00 00 00 00} {06} {00 00 00 00} {06} {00 00 00 00} {06} {00 00 00 00} {06} {00 00 00 00} {06} {00 00 00 00}
    std::string auxDate = "";
    std::string strCSV = "";
    std::string strLog = "";
    bool extract = true;
    std::vector<unsigned char> vFind;
    vFind.push_back(0x02);
    vFind.push_back(0x08);
    vFind.push_back(0x09);
    vFind.push_back(0x0C);

    while (extract ==  true) {

        auxDate = "";

        auto it = std::search(this->vReceivedData.begin(), this->vReceivedData.end(), vFind.begin(), vFind.end());
        if (it != this->vReceivedData.end()) {

            if (this->vReceivedData.size() >= 44) {

                this->vReceivedData.erase(this->vReceivedData.begin(), it + vFind.size());

                // Date
                unsigned short year = vReceivedData[0] << 8 | (vReceivedData[1] & 0xFF);
                int month = (int) vReceivedData[2];
                int day = (int) vReceivedData[3];
                int hour = (int) vReceivedData[5];
                int minute = (int) vReceivedData[6];
                // Bc
                int bc = (int) vReceivedData[13];
                // Ai
                unsigned int ai = vReceivedData[15] << 24 | vReceivedData[15 + 1] << 16 | vReceivedData[15 + 2] << 8 | (vReceivedData[15 + 3] & 0xFF);
                // Ae
                unsigned int ae = vReceivedData[20] << 24 | vReceivedData[20 + 1] << 16 | vReceivedData[20 + 2] << 8 | (vReceivedData[20 + 3] & 0xFF);
                // R1
                unsigned int r1 = vReceivedData[25] << 24 | vReceivedData[25 + 1] << 16 | vReceivedData[25 + 2] << 8 | (vReceivedData[25 + 3] & 0xFF);
                // R2
                unsigned int r2 = vReceivedData[30] << 24 | vReceivedData[30 + 1] << 16 | vReceivedData[30 + 2] << 8 | (vReceivedData[30 + 3] & 0xFF);
                // R3
                unsigned int r3 = vReceivedData[35] << 24 | vReceivedData[35 + 1] << 16 | vReceivedData[35 + 2] << 8 | (vReceivedData[35 + 3] & 0xFF);
                // R4
                unsigned int r4 = vReceivedData[40] << 24 | vReceivedData[40 + 1] << 16 | vReceivedData[40 + 2] << 8 | (vReceivedData[40 + 3] & 0xFF);

                auxDate.append( (day < 10) ? "0" +std::to_string(day) + "/" :std::to_string(day) + "/" );
                auxDate.append( (month < 10) ? "0" +std::to_string(month) + "/" :std::to_string(month) + "/" );
                auxDate.append(to_string(year) + " ");
                auxDate.append( (hour < 10) ? "0" +std::to_string(hour) + ":" :std::to_string(hour) + ":" );
                auxDate.append( (minute < 10) ? "0" +std::to_string(minute) :std::to_string(minute) );

                // Check if the curve is downloaded already
                if (port.checkDownloadedDate(idRequest, auxDate) == false) {
                
                    strCSV = auxDate + ";V;" +std::to_string(ai) + ";" +std::to_string(bc) + ";" +std::to_string(ae) + ";" +std::to_string(bc) + ";" +std::to_string(r1) + ";" +std::to_string(bc) + ";" +std::to_string(r2) + ";" +std::to_string(bc) + ";" +std::to_string(r3) + ";" +std::to_string(bc) + ";" +std::to_string(r4) + ";" +std::to_string(bc) + ";" +std::to_string(bc) + ";" +std::to_string(bc) + ";" +std::to_string(bc) + ";" +std::to_string(bc) + "";
                    strLog = "\n[" + auxDate + "] AI: [" +std::to_string(ai) + "] AE: [" +std::to_string(ae) + "]";

                    // Add the downloaded curve
                    port.addDownloadedDate(idRequest, auxDate);
                    
                    std::string fileNameCSV = port.getLogFile() + to_string(idRequest) + "_TM_incremental_" + this->strDateToStrFileName(port.getInitialDate(idRequest)) + this->strDateToStrFileName(port.getFinalDate(idRequest));

                    File::write(fileNameCSV + ".csv", "\n" + strCSV);
                    File::write(port.getLogFile() + ".log", strLog);

                } else {

                    File::write(port.getLogFile() + ".log", "\n\n +++++ [LOG] [La curva [" + auxDate + "] ya existe] +++++ ");
                    
                    extract = false;

                }
                
                if (this->vReceivedData.size() >= 44) {

                    this->vReceivedData.erase(this->vReceivedData.begin(), this->vReceivedData.begin() + 44);

                }

            } else {

                extract = false;

            }

        } else {

            extract = false;

        }

    }

}

template <class T>
void DLMSController<T>::extractClosure(int idRequest, T &port) {

    File::write(port.getLogFile() + ".log", "\nExtraer cierre: " + Helper::frameToString(this->vReceivedData));

    std::string arrayClosures[7][18];
    std::string strInitialDate = "";
    std::string strFinalDate = "";
    std::string strMaxDate = "";
    std::string strCSV = "";
    std::string auxDate = "";
    unsigned int value = 0;
    std::vector<unsigned char> vDate;

    while (this->vReceivedData.empty() == false) {

        // this->vReceivedData.erase(this->vReceivedData.begin(), this->vReceivedData.begin() + 1);
        
        // Initial date
        for (int i = 0; i < 14; i++) {

            vDate.push_back(vReceivedData[i]);

        }
        strInitialDate = Date::hexDateToStr(vDate);
        vDate.clear();
        this->vReceivedData.erase(this->vReceivedData.begin(), this->vReceivedData.begin() + 14);

        // Final date
        for (int i = 0; i < 14; i++) {

            vDate.push_back(vReceivedData[i]);

        }
        strFinalDate = Date::hexDateToStr(vDate);
        vDate.clear();
        this->vReceivedData.erase(this->vReceivedData.begin(), this->vReceivedData.begin() + 14);

        // Store the initial date, final date, contract and period in the array
        for (int i = 0; i < 7; i++) {

            arrayClosures[i][0] = strInitialDate; // Initial date
            arrayClosures[i][1] = strFinalDate; // Final date
            if (port.getCurveType(idRequest).compare("1") == 0) arrayClosures[i][2] = "1";
            else if (port.getCurveType(idRequest).compare("2") == 0) arrayClosures[i][2] = "2";
            else if (port.getCurveType(idRequest).compare("3") == 0) arrayClosures[i][2] = "3";
            arrayClosures[i][3] = std::to_string(i); 

        }

        // Store values
        int counter = 4;
        for (int i = 0; i < 12; i++) {

            for (int k = 0; k < 7; k++) {

                // {06} {00 00 00 00} 
                value = vReceivedData[1] << 24 | vReceivedData[2] << 16 | vReceivedData[3] << 8 | (vReceivedData[4] & 0xFF);
                arrayClosures[k][counter] =std::to_string(value);
                value = 0;
                vReceivedData.erase(vReceivedData.begin(), vReceivedData.begin() + 5);

            }

            counter = counter + 1;

        }

        counter = 16;
        for (int k = 0; k < 7; k++) {

            // Max value
            // {06} {00 00 00 00} 
            value = vReceivedData[1] << 24 | vReceivedData[2] << 16 | vReceivedData[3] << 8 | (vReceivedData[4] & 0xFF);
            arrayClosures[k][counter] =std::to_string(value);
            value = 0;
            vReceivedData.erase(vReceivedData.begin(), vReceivedData.begin() + 5);

            // Max date
            for (int i = 0; i < 14; i++) {

                vDate.push_back(vReceivedData[i]);

            }
            strMaxDate = Date::hexDateToStr(vDate);
            vDate.clear();
            this->vReceivedData.erase(this->vReceivedData.begin(), this->vReceivedData.begin() + 14);

            arrayClosures[k][counter + 1] = strMaxDate;
            strMaxDate = "";

        }

        for (int k = 0; k < 7; k++) {

            strCSV = "";           

            //
            // arrayClosures structure
            //
            // [0] -> Fhi
            // [1] -> Fhf
            // [2] -> Contract
            // [3] -> Pt 
            // [4] -> AIa
            // [5] -> AEa
            // [6] -> R1a
            // [7] -> R2a
            // [8] -> R3a
            // [9] -> R4a
            // [10] -> AIi
            // [11] -> AEi
            // [12] -> R1i
            // [13] -> R2i
            // [14] -> R3i
            // [15] -> R4i
            // [16] -> Mx
            // [17] -> Fx

            // [0]  -> INICIO 
            strCSV += arrayClosures[k][0] + ";";
            // [1]  -> FIN
            strCSV += arrayClosures[k][1] + ";";
            // [2]  -> CONTRATO
            strCSV += arrayClosures[k][2] + ";";
            // [3]  -> PERIODO
            strCSV += arrayClosures[k][3] + ";";
            // [4]  -> ACTIVA_ABS
            strCSV += arrayClosures[k][4] + ";";
            // [10] -> ACTIVA_INC
            strCSV += arrayClosures[k][10] + ";";
            // INCID_A
            strCSV += "0;";
            // [6] -> REACT1
            strCSV += arrayClosures[k][6] + ";";
            // [12] -> REACT1_I
            strCSV += arrayClosures[k][12] + ";";
            // INCID_R1
            strCSV += "0;";
            // [7] -> REACT2
            strCSV += arrayClosures[k][7] + ";";
            // [13] -> REACT2_I
            strCSV += arrayClosures[k][13] + ";";
            // INCID_R2
            strCSV += "0;";
            // [16] -> MAXPOT
            strCSV += arrayClosures[k][16] + ";";
            // [17] -> FECHAMP
            strCSV += arrayClosures[k][17] + ";";
            // INCID_MAXPOT
            strCSV += "0;";
            // EXCESO
            strCSV += "0;";
            // INCID_EXC
            strCSV += "128";

            // std::cout << "\n" << strCSV;
            
            std::string fileNameCSV = port.getLogFile() + to_string(idRequest) + "_C1_" + port.getCurveType(idRequest) + "_" + this->strDateToStrFileName(port.getInitialDate(idRequest)) + this->strDateToStrFileName(port.getFinalDate(idRequest));
            
            auxDate = arrayClosures[k][0] + " " + arrayClosures[k][1] + " " + arrayClosures[k][2] + " " + arrayClosures[k][3];
            // Add the downloaded closure
            port.addDownloadedDate(idRequest, auxDate);

            File::write(fileNameCSV + ".csv", "\n" + strCSV);

        }

        // Reset the array, variables and vector
        for (int i = 0; i < 18; i++) {

            for (int k = 0; k < 7; k++) {

                arrayClosures[k][i] = "";

            }

        }
        strInitialDate = "";
        strFinalDate = "";
        strMaxDate = "";
        strCSV = "";
        auxDate = "";
        value = 0;
        vDate.clear();

    }

}

template <class T>
std::string DLMSController<T>::getResults() {

    return this->strResults;

}

template <class T>
std::string DLMSController<T>::strDateToStrFileName(std::string strDate) {

    for (std::size_t i = 0; i < strDate.size(); i++) {
    
        if ( (strDate[i] == '/') || (strDate[i] == ' ') || (strDate[i] == ':') ) {
        
            strDate.erase(i,1);
            
        }
        
    }

    return strDate;

}