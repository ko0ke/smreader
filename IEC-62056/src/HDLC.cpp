#include "../include/HDLC.h"

// DEFAULT BYTE
const unsigned char HDLC::DEFAULT_BYTE  = 0x00;

// HDLC INITIAL AND FINAL FLAG FRAME
const unsigned char HDLC::HDLC_FLAG     = 0x7E;

// HDLC FRAME TYPE
const unsigned char HDLC::FRAME_TYPE_3  = 0xA0;

// STATIC HDLC FRAME LENGTH
const unsigned char HDLC::STATIC_HDLC_FRAME_LENGTH  = 0x08;

// HDLC CONTROL FIELDS (CLIENT)
const unsigned char HDLC::CONNECTION_CONTROL_FIELD      = 0x93;
const unsigned char HDLC::DISCONNECTION_CONTROL_FIELD   = 0x53;
const unsigned char HDLC::FIRST_REQUEST_CONTROL_FIELD   = 0x10;
const unsigned char HDLC::MAX_REQUEST_CONTROL_FIELD     = 0x1E;

HDLC::HDLC() {

    this->lastControlField = DEFAULT_BYTE;
    this->dlms.setFrameNumber(DEFAULT_BYTE);

}

std::vector<unsigned char> HDLC::frame(std::string requestType, std::string initalDate, std::string finalDate) {

    std::vector<unsigned char> vFrame;
    std::vector<unsigned char> vDLMS;
    std::vector<unsigned char> vCRC;

    vFrame.push_back(HDLC_FLAG); // Initial byte of the HDLC frame
    vFrame.push_back(FRAME_TYPE_3); // Frame type. Type 3

    // Frame length
    vFrame.push_back(DEFAULT_BYTE); // Number of bytes between initial byte and final byte

    // Destination address.
    vFrame.push_back(0x02);
    vFrame.push_back(0x21);

    // Source address
    vFrame.push_back(0x03);

    // Control field
    
    // Request frame
    if ( (requestType.compare("connection") != 0) && (requestType.compare("disconnection") != 0) ) {

        // First request frame
        if ( (this->lastControlField == DEFAULT_BYTE) || (this->lastControlField == MAX_REQUEST_CONTROL_FIELD) ) {

            vFrame.push_back(FIRST_REQUEST_CONTROL_FIELD);

        }
        // Second or more 
        else {
            
            vFrame.push_back(lastControlField + 0x02); 

        }

        // Set the frame length
        vDLMS = dlms.frame(requestType, initalDate, finalDate);
        
        // Bytes that we send without 7E flags (10 + DLMS frame bytes)
        vFrame[2] = (vDLMS.size() + 10);

        // Calculate CRC (HCS)
        vCRC = this->calculateCRC("HCS", vFrame);
        vFrame.push_back(vCRC[0]);
        vFrame.push_back(vCRC[1]);
        vCRC.clear();

        // Add the DLMS PDU into HDLC frame
        for (std::size_t i = 0; i < vDLMS.size(); i++) {

            vFrame.push_back(vDLMS[i]);

        }
        

    } else {
    
        // Connection frame
        if (requestType.compare("connection") == 0) {

            vFrame.push_back(CONNECTION_CONTROL_FIELD); // Connection flag. Flag SNRM (Set Normal Response Mode)

        } 
        // Disconnection frame
        else if (requestType.compare("disconnection") == 0) {

            vFrame.push_back(DISCONNECTION_CONTROL_FIELD); // Disconnection flag. DISC command

        }

        // Set the frame length
        vFrame[2] = STATIC_HDLC_FRAME_LENGTH;

    } 

    // FCS
    vCRC = this->calculateCRC("FCS", vFrame);
    vFrame.push_back(vCRC[0]);
    vFrame.push_back(vCRC[1]);
    vCRC.clear();

    // Final byte of the HDLC frame
    vFrame.push_back(HDLC_FLAG); 


    return vFrame;

}

void HDLC::setLastControlField(unsigned char lastControlField) {

    this->lastControlField = lastControlField;

}

void HDLC::setFrameNumber(unsigned int frameNumber) {

    this->dlms.setFrameNumber(frameNumber);

}

std::vector<unsigned char> HDLC::calculateCRC(std::string crcType, std::vector<unsigned char> vFrame) {

    std::vector<unsigned char> vCRC;
    unsigned int result = 0;

    if (crcType.compare("HCS") == 0) {

        // Calculate the two bytes of the CRC header. From the second byte [0xA0] to the sixth [Control field]
        unsigned char hcs[] = {vFrame[1], vFrame[2], vFrame[3], vFrame[4], vFrame[5], vFrame[6]};
        result = CRC::Calculate(hcs, sizeof(hcs), CRC::CRC_16_X25());

    } else if(crcType.compare("FCS") == 0) {

        // Calculate the two bytes of the frame. From the second byte [0xA0] to the last byte.
        unsigned char fcs[vFrame.size() - 1];
        for (std::size_t i = 0; i < vFrame.size() - 1; i++) {
            fcs[i] = vFrame[i + 1];
        }
        result = CRC::Calculate(fcs, sizeof(fcs), CRC::CRC_16_X25());

    }
    vCRC.push_back((unsigned int)((result & 0XFF)));         
    vCRC.push_back((unsigned int)((result >> 8) & 0XFF));    // Two CRC bytes

    return vCRC;

}
