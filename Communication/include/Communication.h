#ifndef _communication_h_
#define _communication_h_

#include "../../include/File.h"
#include "../../include/Date.h"

#include <string>
#include <vector>
#include <tuple>
#include <iostream>
#include <algorithm>

class Communication {
    
    protected:

        // Communication parameters
        
        // GPRS Communication
        std::string ip;
        unsigned short port;

        // GSM Communication
        std::string phoneNumber;
        std::string comm;

        int defaultTimeout;
        int currentTimeout;

        // IEC parameters
        int linkAddress;
        int measurementPoint;

        // IEC and DLMS parameters
        int password;

        // Protocol
        std::string protocol;

        // Operation
        std::string operation;

        // Request
        // tuple< int tryNumber, int curve, std::string curveType, std::string initialDate, std::string finalDate, std::vector<std::string> downloadedDates, int code, std::string message, std::string startCall, std::string endCall>
        std::vector< std::tuple<int, int, std::string, std::string, std::string, std::vector<std::string>, int, std::string, std::string, std::string> > vRequests;

        std::string logFile;

        std::string dateTimeMeter;
        std::string dateTimeServer;
        // ¿other requests?
        
    public:

        Communication();

        //     #####################
        //     ##  COMMUNICATION  ##
        //     #####################
        
        virtual bool open() = 0;
        virtual void sendFrame(std::vector<unsigned char>) = 0;
        virtual std::vector<unsigned char> receiveFrame() = 0;
        virtual void close() = 0;

        //     ##############
        //     ##  SOCKET  ##
        //     ##############

        std::string getIp();
        unsigned short getPort();

        //     ##############
        //     ##  SERIAL  ##
        //     ##############

        std::string getPhoneNumber();
        std::string getCOMM();

        //     ###########################
        //     ##  MODEM CONFIGURATION  ##
        //     ###########################        

        int getDefaultTimeout();
        virtual void updateTimeout(int) = 0;
        void resetTimeout();

        std::string getProtocol();
        virtual std::string getDerivedClassName() = 0;

        //     ###########################
        //     ##  IEC/DLMS PARAMETERS  ##
        //     ###########################

        int getLinkAddress();
        int getMeasurementPoint();
        int getPassword();

        //     #################
        //     ##  OPERATION  ##
        //     #################

        std::string getOperation();

        //     ##############
        //     ##  REQUEST ##
        //     ##############

        int getTotalRequests();

        int getTryNumber(int);
        int getCurve(int);
        std::string getCurveType(int);
        std::string getInitialDate(int);
        std::string getFinalDate(int);
        // Downloaded Dates
        // Add a new date
        void addDownloadedDate(int, std::string);
        // Check if the date exists
        bool checkDownloadedDate(int, std::string);
        std::string getLastDownloadedDate(int);
        int getTotalDownloadedDates(int);

        int getCode(int);
        std::string getMessage(int);
        std::string getStartCall(int);
        std::string getEndCall(int);

        std::string getLogFile();

        std::string getDateTimeMeter();
        std::string getDateTimeServer();

        // Add new Request
        void addRequest(int, std::string, std::string, std::string);
                
        void setLinkAddress(int);
        void setMeasurementPoint(int);
        void setPassword(int);
        void setProtocol(std::string);
        void setOperation(std::string);

        void setTryNumber(int, int);
        void setCode(int, int);
        void setMessage(int, std::string);
        void setStartCall(int, std::string);
        void setEndCall(int, std::string);

        void setLogFile(std::string);

        void setDateTimeMeter(std::string);
        void setDateTimeServer(std::string);

};

#endif