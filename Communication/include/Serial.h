#ifndef _serial_h_
#define _serial_h_

#include "Communication.h"

#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <chrono>
#include <iomanip>

#ifdef __linux__ 

#include <termios.h>
#include <unistd.h>
#include <fcntl.h>

#elif __WIN32__ || __WIN64__

// Serial
#include <WinBase.h>

#endif

class Serial : public Communication {

    private:

#ifdef __linux__

            int fdPuerto;

#elif __WIN32__ || __WIN64__

            DCB dcb;
            HANDLE hPuerto;

#endif

        bool isOpen;

        void sendFrame(std::string);

        bool receiveFrame(std::string);

    public:

        Serial();

        Serial(const Serial&);

        Serial(std::string);

        bool open() override;
        void sendFrame(std::vector<unsigned char>) override;
        std::vector<unsigned char> receiveFrame() override;
        void close() override;

        std::string getDerivedClassName() override;
        void updateTimeout(int) override;

        //     ###############
        //     ##  SETTERS  ##
        //     ###############

        void setPhoneNumber(std::string);
        void setCOMM(std::string);

        void setTimeout(int);
        void setBaudRate(int);
        void setByteSize(int);
        void setParity(int);
        void setStopBits(int);

        void reset();

};

#endif