#ifndef _socket_h_
#define _socket_h_

#include "Communication.h"

#include <iostream>
#include <vector>
#include <string>

#ifdef __linux__ 

    // Linux code

#elif __WIN32__ || __WIN64__

    // Socket
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <Windows.h>

#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

#endif

class Socket : public Communication {

    private:

#ifdef __linux__ 

    // TODO: Linux code

#elif __WIN32__ || __WIN64__

        SOCKET fdSocket;

#endif 

        

    public:

        Socket();
        Socket(const Socket&);

        bool open() override;
        void sendFrame(std::vector<unsigned char>) override;
        std::vector<unsigned char> receiveFrame() override;
        void close() override;
        
        std::string getDerivedClassName() override;
        void updateTimeout(int) override;

        //     ###############
        //     ##  SETTERS  ##
        //     ###############

        void setIp(std::string);
        void setPort(unsigned short);
        void setTimeout(int);

        void reset();

};

#endif