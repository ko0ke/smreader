#include "../include/Communication.h"

Communication::Communication() { }

//     ##############
//     ##  SOCKET  ##
//     ##############

std::string Communication::getIp() {

    return this->ip;

}

unsigned short Communication::getPort() {

    return this->port;

}

//     ##############
//     ##  SERIAL  ##
//     ##############

std::string Communication::getPhoneNumber() {

    return this->phoneNumber;

}

std::string Communication::getCOMM() {

    return this->comm;

}

//     ###########################
//     ##  MODEM CONFIGURATION  ##
//     ###########################        

int Communication::getDefaultTimeout() {

    return this->defaultTimeout;

}

void Communication::resetTimeout() {

    this->currentTimeout = this->getDefaultTimeout();

}

std::string Communication::getProtocol()  {

    return this->protocol;

}

//     #################
//     ##  OPERATION  ##
//     #################

std::string Communication::getOperation() {

    return this->operation;

}

//     ###########################
//     ##  IEC/DLMS PARAMETERS  ##
//     ###########################

int Communication::getLinkAddress() {

    return this->linkAddress;

}

int Communication::getMeasurementPoint() {

    return this->measurementPoint;

}

int Communication::getPassword() {

    return this->password;

}

//     ##############
//     ##  REQUEST ##
//     ##############

    //     #############
    //     ##  GETTER ##
    //     #############

int Communication::getTotalRequests() {

    return this->vRequests.size();

}

int Communication::getTryNumber(int idRequest) {

    return std::get<0>(this->vRequests.at(idRequest));

}

int Communication::getCurve(int idRequest) {

    return std::get<1>(this->vRequests.at(idRequest));

}

std::string Communication::getCurveType(int idRequest) {

    return std::get<2>(this->vRequests.at(idRequest));

}

std::string Communication::getInitialDate(int idRequest) {

    return std::get<3>(this->vRequests.at(idRequest));

}

std::string Communication::getFinalDate(int idRequest) {

    return std::get<4>(this->vRequests.at(idRequest));

}

// Downloaded Dates
// Add a new date
void Communication::addDownloadedDate(int idRequest, std::string downloadedDate) {

    std::get<5>(this->vRequests.at(idRequest)).push_back(downloadedDate);

}

// Check if the date exists
bool Communication::checkDownloadedDate(int idRequest, std::string downloadedDate) {

    bool exists = false;

    if (std::find(std::get<5>(this->vRequests.at(idRequest)).begin(), std::get<5>(this->vRequests.at(idRequest)).end(), downloadedDate) != std::get<5>(this->vRequests.at(idRequest)).end()) {

        exists = true;

    }

    return exists;

}

// Get the last downloaded date
std::string Communication::getLastDownloadedDate(int idRequest) {

    std::string lastDate = "";

    if (std::get<5>(this->vRequests.at(idRequest)).empty() == false) {

        lastDate = std::get<5>(this->vRequests.at(idRequest)).back();

    }

    if (lastDate.compare("") == 0) {

        lastDate = this->getInitialDate(idRequest);

    }

    return lastDate;

}

int Communication::getTotalDownloadedDates(int idRequest) {

    return std::get<5>(this->vRequests.at(idRequest)).size();

}

int Communication::getCode(int idRequest) {

    return std::get<6>(this->vRequests.at(idRequest));

}

std::string Communication::getMessage(int idRequest) {

    return std::get<7>(this->vRequests.at(idRequest));

}

std::string Communication::getStartCall(int idRequest) {

    return std::get<8>(this->vRequests.at(idRequest));

}

std::string Communication::getEndCall(int idRequest) {

    return std::get<9>(this->vRequests.at(idRequest));

}

std::string Communication::getLogFile() {

    return this->logFile;

}

std::string Communication::getDateTimeMeter() {

    return this->dateTimeMeter;

}
std::string Communication::getDateTimeServer() {

    return this->dateTimeServer;
}

// Add new Request
void Communication::addRequest(int curve, std::string curveType, std::string initialDate, std::string finalDate) {
    
    this->vRequests.push_back( std::tuple<int, int, std::string, std::string, std::string, std::vector<std::string>, int, std::string, std::string, std::string>(0, curve, curveType, initialDate, finalDate, {}, 0, "Sin errores. Todo OK.", "", "") );

}

    //     #############
    //     ##  SETTER ##
    //     #############

void Communication::setTryNumber(int idRequest, int tryNumber) {

    std::get<0>(this->vRequests.at(idRequest)) = tryNumber;

}

void Communication::setCode(int idRequest, int code) {

    std::get<6>(this->vRequests.at(idRequest)) = code;

}

void Communication::setMessage(int idRequest, std::string message) {

    std::get<7>(this->vRequests.at(idRequest)) = message;

}

void Communication::setStartCall(int idRequest, std::string startCall) {
 
    std::get<8>(this->vRequests.at(idRequest)) = startCall;

}

void Communication::setEndCall(int idRequest, std::string endCall) {

    std::get<9>(this->vRequests.at(idRequest)) = endCall;

}

void Communication::setLogFile(std::string logFile) {

    this->logFile = logFile;

}

void Communication::setLinkAddress(int linkAddress) {

    this->linkAddress = linkAddress;

}

void Communication::setMeasurementPoint(int measurementPoint) {

    this->measurementPoint = measurementPoint;

}

void Communication::setPassword(int password) {

    this->password = password;

}

void Communication::setProtocol(std::string protocol) {

    this->protocol = protocol;

}

void Communication::setOperation(std::string operation) {

    this->operation = operation;

}

void Communication::setDateTimeMeter(std::string dateTimeMeter) {

    this->dateTimeMeter = dateTimeMeter;

}

void Communication::setDateTimeServer(std::string dateTimeServer) {

    this->dateTimeServer = dateTimeServer;

}