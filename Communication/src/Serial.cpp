#include "../include/Serial.h"

Serial::Serial() { }

Serial::Serial(std::string comm) {

    this->comm = comm;

}

Serial::Serial(const Serial& auxSerial) {

    // Connection parameters
    this->phoneNumber = auxSerial.phoneNumber;
    this->comm = auxSerial.comm;
    this->defaultTimeout = auxSerial.defaultTimeout;
    this->currentTimeout = auxSerial.currentTimeout;

    // IEC parameters
    this->linkAddress = auxSerial.linkAddress;
    this->measurementPoint = auxSerial.measurementPoint;

    // IEC and DLMS parameters
    this->password = auxSerial.password;

    // Protocol
    this->protocol = auxSerial.protocol;

    // Operation
    this->operation = auxSerial.operation;

    // Requests
    this->vRequests = auxSerial.vRequests;

    this->logFile = auxSerial.logFile;

    this->dateTimeMeter = auxSerial.dateTimeMeter;

    this->dateTimeServer = auxSerial.dateTimeServer;

}

bool Serial::open() {
#ifdef __linux__

    // TODO: Linux code 

    this->fdPuerto = open((char*)this->comm.c_str(), O_RDWR | O_NOCTTY | O_NDELAY);
    if (this->fdPuerto < 0) {

        // TODO: Set error
        this->isOpen = false;

    } else {

        struct termios options;

        // Clear all flags on the file descriptor
        fcntl(this->fdPuerto, F_SETFL, 0);
        tcgetattr(this->fdPuerto, &options);

        /*
            B0
            B50
            B75
            B110
            B134
            B150
            B200
            B300
            B600
            B1200
            B1800
            B2400
            B4800
            B9600
            B19200
            B38400
            B57600
            B115200
            B230400
        */

        // TODO: Set BaudRate
        cfsetispeed(&options, B9600);
        cfsetospeed(&options, B9600);
        // TODO: Set 8N1
        options.c_cflag &= ~PARENB; // Clear parity bit, disabling parity
        options.c_cflag &= ~CSTOPB; // Clear stop field, only one stop bit used in communication
        options.c_cflag |= CS8; // 8 bits per byte
        options.c_cflag != ~CRTSCTS; // Disable RTS/CTS hardware flow control
        options.c_cflag |= CREAD | CLOCAL; // Turn on READ & ignore ctrl lines (CLOCAL = 1)
        options.c_lflag &= ~ICANON; // Canonical mode disabled
        options.c_lflag &= ~ECHO; // Disable echo
        options.c_lflag &= ~ECHOE; // Disable erasure
        options.c_lflag &= ~ECHONL; // Disable new-line echo
        options.c_lflag &= ~ISIG; // Disable interpretation of INTR, QUIT and SUSP
        options.c_iflag &= ~(IXON | IXOFF | IXANY); // Turn off s/w flow ctrl
        options.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL); // Disable any special handling of received bytes
        options.c_oflag &= ~OPOST; // Prevent special interpretation of output bytes
        options.c_oflag &= ~ONLCR; // Prevent conversion of newline to carriage return/line feed
        options.c_cc[VMIN]  = 0;
        // 0 to 25.5 seconds
        options.c_cc[VTIME] = this->timeout; // Inmediate or after 0.5 seconds

        tcflush(this->fdPuerto, TCIFLUSH);
        if(tcsetattr(this->fdPuerto, TCSANOW, &options) != 0) {
            
            // TODO: Set error
            this->isOpen = false;

        } else {

            // * After opening the device, use ioctl(fd, TIOCEXCL) to put the serial port into exclusive mode. 
            // * Until the descriptor is closed, or the process issues ioctl(fd, TIOCNXCL), 
            // * any attempt to open the device will fail with EBUSY error code.

        }

    }

#elif __WIN32__ || __WIN64__

    hPuerto = CreateFile((char*)this->getCOMM().c_str(), GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);
    if (hPuerto == INVALID_HANDLE_VALUE) {

        for (std::size_t i = 0; i < this->vRequests.size(); i++) {
                    
            this->setCode(i, 1);
            this->setMessage(i, "No se ha podido abrir el puerto: " + this->getCOMM());
        
        }

        File::write(this->getLogFile() + ".log", "\n\n +++++ [LOG] No se ha podido abrir el puerto: " + this->getCOMM() + " +++++\n\n");

        this->isOpen = false;

    } else {

        this->dcb.DCBlength = sizeof(DCB);
        
        if (GetCommState(this->hPuerto, &this->dcb) == false) {

            this->isOpen = false;
            File::write(this->getLogFile() + ".log", "\n\n +++++ [LOG] No se ha podido abrir el puerto: " + this->getCOMM() + " +++++\n\n");

        } else {

            // BaudRate
            /*
                CBR_110
                CBR_300
                CBR_600
                CBR_1200
                CBR_2400
                CBR_4800
                CBR_9600
                CBR_14400
                CBR_19200
                CBR_38400
                CBR_57600
                CBR_115200
                CBR_128000
                CBR_256000
            */

            // Paridad
            // EVENPARITY
            // MARKPARITY
            // NOPARITY
            // ODDPARITY
            // SPACEPARITY

            // Bit Parada  
            // ONESTOPBIT  1 stop bit.
            // ONE5STOPBITS    1.5 stop bits.
            // TWOSTOPBITS 2 stop bits. 

            //  9600 bps, 8 data bits, no parity, and 1 stop bit.
            this->dcb.BaudRate = CBR_9600;      //  baud rate
            this->dcb.ByteSize = 8;             //  data size, xmit and rcv
            this->dcb.Parity   = NOPARITY;      //  parity bit
            this->dcb.StopBits = ONESTOPBIT;    //  stop bit

            //  Establecemos la nueva configuracion 
            if (SetCommState(this->hPuerto, &this->dcb) == false) {
                
                this->isOpen = false;
                
                File::write(this->getLogFile() + ".log", "\n[ERROR] No se ha podido abrir el puerto: " + this->getCOMM() + "\n");

            } else {
                
                this->updateTimeout(0);
                // Call
                this->sendFrame("AT");
                if (this->receiveFrame("OK") == true) {
                    
                    sendFrame("ATDT" + this->getPhoneNumber());
                    if (receiveFrame("CONNECT") == false) {
                        
                        this->isOpen = false;

                    } else {

                        this->isOpen = true;

                    }

                } else {

                    this->isOpen = false;

                }


            }

            

        }

    }

#endif

    return this->isOpen;

}

void Serial::sendFrame(std::vector<unsigned char> vFrame) {

    char frame[vFrame.size()];
    // Convertimos el vector en un char array para poder enviarlo
    copy(vFrame.begin(), vFrame.end(), frame);
    bool sent = true;
    
#ifdef __linux__

    // TODO: Linux code

#elif __WIN32__ || __WIN64__

    DWORD dwBytesWritten = 0;

    if (WriteFile(this->hPuerto, frame, sizeof(frame), &dwBytesWritten, NULL) != true) {

        sent = false;

    }

    if (sent == false) {

        File::write(this->getLogFile() + ".log", "\n[ERROR] La trama no se ha podido enviar.");

    } else {

        File::write(this->getLogFile() + ".log", "\n[ENVIADO] -> " + Helper::frameToString(vFrame));
    
    }

#endif
}

void Serial::sendFrame(std::string command) {
#ifdef __linux__
#elif __WIN32__ || __WIN64__

    DWORD dwBytesWritten = 0;
    command += "\r";
    int n = command.length();
    char buffer[command.length() + 1];
    // Convert the string into a char array
    strcpy(buffer, command.c_str()); 

    if (WriteFile(this->hPuerto, buffer, strlen(buffer), &dwBytesWritten, NULL) != true) {

        File::write(this->getLogFile() + ".log", "\n[ERROR] El comando AT no se ha podido enviar");

    }

#endif
}

std::vector<unsigned char> Serial::receiveFrame() {

    std::vector<unsigned char> vFrame;
    unsigned char buffer;
    int counter = 0;
    // max buffer size
    int maxCounter = 1000;
    bool read = true;

#ifdef __linux__
#elif __WIN32__ || __WIN64__

    DWORD dwRead;
    
    while (read == true) {

        ReadFile(this->hPuerto, &buffer, 1, &dwRead, NULL);
        if (dwRead == 1) {
            
            vFrame.push_back(buffer);

            counter = counter + 1;

            if (counter >= maxCounter) {

                read = false;
                vFrame.clear();

            }

        } else {

            read = false;

        }

    }

    File::write(this->getLogFile() + ".log", "\n\n[RECIBIDO] <- " + Helper::frameToString(vFrame));

#endif

    return vFrame;

}

bool Serial::receiveFrame(std::string command) {

    bool connected = true;

#ifdef __linux__

    // TODO: Linux code

#elif __WIN32__ || __WIN64__

    bool isConnected = false; 
    unsigned char buffer;
    std::vector<unsigned char> response;
    std::vector<std::string> errors;
    errors.push_back("CARRIER");
    errors.push_back("DIALTONE");
    errors.push_back("BUSY");
    errors.push_back("ERROR");
    DWORD dwRead;
    bool read = true;
    int counter = 0;

    File::write(this->getLogFile() + ".log", "\n");

    while (read == true) {
        
        ReadFile(this->hPuerto, &buffer, 1, &dwRead, NULL);
        if (dwRead == 1) {
            
            response.push_back(buffer);

        } else {
            
            std::string str(response.begin(), response.end());
            
            if(command.compare("OK") == 0) {
                
                int found = str.find(command);
                if (found != std::string::npos) {
                    
                    File::write(this->getLogFile() + ".log", "\nLlamando...");

                    isConnected = true;
                    read = false;
                    break;

                }

            }
            
            if(command.compare("CONNECT") == 0) {
                
                int found = str.find(command);
                if (found != std::string::npos) {

                    File::write(this->getLogFile() + ".log", "Conectado!");
                    
                    // Set Initial Date and Hour
                    for (std::size_t i = 0; i < this->vRequests.size(); i++) {

                        this->setStartCall(i, Date::now());

                    }

                    isConnected = true;
                    read = false;
                    break;

                }

            }

            counter = counter + 1;
            
            for (std::size_t i = 0; i < errors.size(); i++) {

                if(errors[i].compare("CARRIER") == 0) {
                    
                    int found = str.find(errors[i]);
                    if (found != std::string::npos) {

                        for (std::size_t i = 0; i < this->vRequests.size(); i++) {
                    
                            this->setCode(i, 2);
                            this->setMessage(i, "El modem remoto no responde (GMS). NO CARRIER.");
                        
                        }

                        File::write(this->getLogFile() + ".log", "\n\n +++++ [LOG] El modem remoto no responde (GMS). NO CARRIER. +++++\n\n");
                        
                        read = false;
                        break;

                    }

                } else if(errors[i].compare("DIALTONE") == 0) {
                    
                    int found = str.find(errors[i]);
                    if (found != std::string::npos) {

                        for (std::size_t i = 0; i < this->vRequests.size(); i++) {
                    
                            this->setCode(i, 2);
                            this->setMessage(i, "El modem remoto no responde (GMS). NO DIALTONE.");
                        
                        }

                        File::write(this->getLogFile() + ".log", "\n\n +++++ [LOG] El modem remoto no responde (GMS). NO DIALTONE. +++++\n\n");

                        read = false;
                        break;

                    }

                } else if(errors[i].compare("BUSY") == 0) {
                    
                    int found = str.find(errors[i]);
                    if (found != std::string::npos) {

                        for (std::size_t i = 0; i < this->vRequests.size(); i++) {
                    
                            this->setCode(i, 2);
                            this->setMessage(i, "El modem remoto no responde (GMS). BUSY.");
                        
                        }

                        File::write(this->getLogFile() + ".log", "\n\n +++++ [LOG] El modem remoto no responde (GMS). BUSY. +++++\n\n");
                        
                        read = false;
                        break;

                    }

                } else if(errors[i].compare("ERROR") == 0) {
                    
                    int found = str.find(errors[i]);
                    if (found != std::string::npos) {

                        for (std::size_t i = 0; i < this->vRequests.size(); i++) {
                    
                            this->setCode(i, 2);
                            this->setMessage(i, "El modem remoto no responde (GMS). ERROR.");
                        
                        }

                        File::write(this->getLogFile() + ".log", "\n\n +++++ [LOG] El modem remoto no responde (GMS). ERROR. +++++\n\n");
                        
                        read = false;
                        break;

                    }

                }

            }

            if (counter == 30) {

                for (std::size_t i = 0; i < this->vRequests.size(); i++) {
                    
                    this->setCode(i, 2);
                    this->setMessage(i, "El modem remoto no responde (GMS). TIMEOUT.");
                
                }

                File::write(this->getLogFile() + ".log", "\n\n +++++ [LOG] El modem remoto no responde (GMS). TIMEOUT. +++++\n\n");
                
                read = false;

            }

        }

    }
    
    return isConnected;

#endif

    return connected;

}

void Serial::close() {

#ifdef __linux__

    // TODO: Linux code

#elif __WIN32__ || __WIN64__

    Sleep(1000);
    sendFrame("+++");
    Sleep(1000);
    sendFrame("ATH");

    // Set end Date and Hour
    for (std::size_t i = 0; i < this->vRequests.size(); i++) {

        this->setEndCall(i, Date::now());

    }

    CloseHandle(this->hPuerto);
    
    File::write(this->getLogFile() + ".log", "\n\nCerrado!");  

#endif

}

void Serial::updateTimeout(int timeout) {

#ifdef __linux__ 

    // TODO: Linux code

#elif  __WIN32__ || __WIN64__

    int newTimeout = this->currentTimeout + timeout;
    this->currentTimeout = newTimeout;

    File::write(this->getLogFile() + ".log", "\nTimeout actualizado: " + std::to_string(this->currentTimeout));
    // Aplicamos un timeout al serial
    COMMTIMEOUTS timeouts = { 0 };
    GetCommTimeouts(this->hPuerto,&timeouts);
    timeouts.ReadIntervalTimeout = this->currentTimeout;
    timeouts.ReadTotalTimeoutMultiplier = this->currentTimeout;
    timeouts.ReadTotalTimeoutConstant = this->currentTimeout;
    timeouts.WriteTotalTimeoutMultiplier = this->currentTimeout;
    timeouts.WriteTotalTimeoutConstant = this->currentTimeout;
    // Establecemos los timeout a la configuracion
    SetCommTimeouts(this->hPuerto, &timeouts);

#endif

}

std::string Serial::getDerivedClassName() {

    return "Serial";

}

    //     ###############
    //     ##  SETTERS  ##
    //     ###############

void Serial::setPhoneNumber(std::string phoneNumber) {

    this->phoneNumber = phoneNumber;

}

void Serial::setCOMM(std::string comm) {

    this->comm = comm;

}

void Serial::setTimeout(int timeout) {

    this->defaultTimeout = timeout;
    this->currentTimeout = timeout;

}

void Serial::setBaudRate(int baudRate) { }

void Serial::setByteSize(int byteSize) { }

void Serial::setParity(int parity) { }

void Serial::setStopBits(int stopBits) { }

void Serial::reset() {

    // Connection parameters
    this->phoneNumber = "";
    this->comm = "";
    this->defaultTimeout = 0;
    this->currentTimeout = 0;

    // IEC parameters
    this->linkAddress = 0;
    this->measurementPoint = 0;

    // IEC and DLMS parameters
    this->password = 0;

    // Protocol
    this->protocol = "";

    // Operation
    this->operation = "";

    // Requests
    this->vRequests.clear();

    this->logFile = "";

}