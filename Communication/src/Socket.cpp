#include "../include/Socket.h"

Socket::Socket() { }

Socket::Socket(const Socket& auxSocket) {

    // Connection parameters
    this->ip = auxSocket.ip;
    this->port = auxSocket.port;
    this->defaultTimeout = auxSocket.defaultTimeout;
    this->currentTimeout = auxSocket.currentTimeout;

    // IEC parameters
    this->linkAddress = auxSocket.linkAddress;
    this->measurementPoint = auxSocket.measurementPoint;

    // IEC and DLMS parameters
    this->password = auxSocket.password;

    // Protocol
    this->protocol = auxSocket.protocol;

    // Operation
    this->operation = auxSocket.operation;

    // Requests
    this->vRequests = auxSocket.vRequests;

    this->logFile = auxSocket.logFile;

    this->dateTimeMeter = auxSocket.dateTimeMeter;

    this->dateTimeServer = auxSocket.dateTimeServer;

}

bool Socket::open() {
    
    bool connected = true;

#ifdef __linux__ 

    // TODO: Linux code

#elif  __WIN32__ || __WIN64__

    // Socket
    WSADATA wsaData;
    struct sockaddr_in sockAddress;
    this->fdSocket = INVALID_SOCKET;
    int iResult;

    // WSAStartup function initiates use of the Winsock DLL
    iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (iResult != 0) {
        
        for (std::size_t i = 0; i < this->vRequests.size(); i++) {
                    
            this->setCode(i, 3);
            this->setMessage(i, "No se ha podido abrir el socket. [Winsock]");
        
        }

        File::write(this->getLogFile() + ".log", "\n\n +++++ [LOG] No se ha podido abrir el socket. [Winsock] +++++\n\n");

        connected = false;

    } else {
        
        // Create a SOCKET for connecting to server
        // AF_INET -> The Internet Protocol version 4 (IPv4) address family
        // SOCK_STREAM -> Transmission Control Protocol (TCP) for AF_INET
        // 0 -> The service provider will choose the protocol to use.
        if((this->fdSocket = socket(AF_INET, SOCK_STREAM, 0)) == INVALID_SOCKET) {

            for (std::size_t i = 0; i < this->vRequests.size(); i++) {
                    
                this->setCode(i, 3);
                this->setMessage(i, "No se ha podido abrir el socket. [Socket]");
            
            }

            File::write(this->getLogFile() + ".log", "\n\n +++++ [LOG] No se ha podido abrir el socket. [Socket] +++++\n\n");

            WSACleanup();
            connected = false;

        } else {

            this->updateTimeout(0);

            // Define the internet address
            sockAddress.sin_addr.s_addr = inet_addr(this->getIp().c_str()); // IP address
            sockAddress.sin_family = AF_INET; // TCP/IP Family
            sockAddress.sin_port = htons(this->getPort()); // Port number

            // Connect to server
            if (connect(this->fdSocket, (struct sockaddr *)&sockAddress, sizeof(sockAddress)) < 0) {

                for (std::size_t i = 0; i < this->vRequests.size(); i++) {
                    
                    this->setCode(i, 4);
                    this->setMessage(i, "El modem remoto no responde (IP).");
                
                }
                
                File::write(this->getLogFile() + ".log", "\n\n +++++ [LOG] El modem remoto no responde (IP). +++++\n\n");

                connected = false;

            } else {
                
                File::write(this->getLogFile() + ".log", "\nConectado!");

                // Set Initial Date and Hour
                for (int i = 0; i < this->vRequests.size(); i++) {

                    this->setStartCall(i, Date::now());

                }
                
            }

        }
    } 

#endif

    return connected;

}

void Socket::sendFrame(std::vector<unsigned char> vFrame) {

    char frame[vFrame.size()];
    // Convert the vector to char array
    std::copy(vFrame.begin(), vFrame.end(), frame);
    bool sent = true;

    int sentBytes = 0;
    // Send the frame
    sentBytes = send(this->fdSocket, frame, sizeof(frame), 0);
    if (sentBytes == -1) {

        sent = false;
        
    }

    if (sent == false) {

        File::write(this->getLogFile() + ".log", "\n[ERROR] La trama no se ha podido enviar.");

    } else {

        File::write(this->getLogFile() + ".log", "\n[ENVIADO] -> " + Helper::frameToString(vFrame));
    
    }

}

std::vector<unsigned char> Socket::receiveFrame() {

    std::vector<unsigned char> vFrame;

    int counter = 0;
    // Maximo de bytes que se puede recibir en una sola trama
    int maxCounter = 1000;

    char byte;        
    while (recv(this->fdSocket, &byte, 1, 0) == 1) {

        vFrame.push_back(byte);

        counter = counter + 1;

        if (counter >= maxCounter) {

            vFrame.clear();
            break;

        }

    }

    File::write(this->getLogFile() + ".log", "\n\n[RECIBIDO] <- " + Helper::frameToString(vFrame));

    return vFrame;

}

void Socket::close() {

#ifdef __linux__ 

    // TODO: Linux code

#elif  __WIN32__ || __WIN64__

    int iResult = closesocket(this->fdSocket);
    if (iResult == SOCKET_ERROR) {
        
        WSACleanup();

    }

#endif

    File::write(this->getLogFile() + ".log", "\n\nCerrado!");

    // Set end Date and Hour
    for (std::size_t i = 0; i < this->vRequests.size(); i++) {

        this->setEndCall(i, Date::now());

    }

}

void Socket::updateTimeout(int timeout) {

#ifdef __linux__ 

    // TODO: Linux code

#elif  __WIN32__ || __WIN64__

    int newTimeout = this->currentTimeout + timeout;
    this->currentTimeout = newTimeout;

    File::write(this->getLogFile() + ".log", "\nTimeout actualizado: " + std::to_string(this->currentTimeout));
    setsockopt(this->fdSocket, SOL_SOCKET, SO_RCVTIMEO, (char *)&this->currentTimeout, sizeof(int));

#endif

}

std::string Socket::getDerivedClassName() {

    return "Socket";

}

    //     ###############
    //     ##  SETTERS  ##
    //     ###############

void Socket::setIp(std::string ip) {

    this->ip = ip;

}

void Socket::setPort(unsigned short port) {

    this->port = port;

}

void Socket::setTimeout(int timeout) {

    this->defaultTimeout = timeout;
    this->currentTimeout = timeout;

}

void Socket::reset() {

    // Connection parameters
    this->ip = "";
    this->port = 0;
    this->defaultTimeout = 0;
    this->currentTimeout = 0;

    // IEC parameters
    this->linkAddress = 0;
    this->measurementPoint = 0;

    // IEC and DLMS parameters
    this->password = 0;

    // Protocol
    this->protocol = "";

    // Operation
    this->operation = "";

    // Requests
    this->vRequests.clear();

    this->logFile = "";

}