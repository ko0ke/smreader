#include "../include/File.h"

const std::string File::HEADER_CURVE_CSV = "TIMESTAMP;NHORA;ACTIVA_I;INCID1;ACTIVA_E;INCID2;REACTQ1;INCID3;REACTQ2;INCID4;REACTQ3;INCID5;REACTQ4;INCID6;RESERVA1;INCID7;RESERVA2;INCID8";
const std::string File::HEADER_CLOSURE_CSV = "INICIO;FIN;CONTRATO;PERIODO;ACTIVA_ABS;ACTIVA_INC;INCID_A;REACT1;REACT1_I;INCID_R1;REACT2;REACT2_I;INCID_R2;MAXPOT;FECHAMP;INCID_MAXPOT;EXCESO;INCID_EXC";


File::File() {

}

void File::write(std::string strFileName, std::string strLine) {

    // Check if the file doesn't exist to add the header
    if (File::checkFile(strFileName) == false) {

        // Open the file
        ofstream oFile;
        oFile.open(strFileName, ios_base::app);
        
        // Check if the file is opened
        if (oFile.is_open()) {

            // Closure file
            std::size_t found = strFileName.find("_C1");
            if (found != std::string::npos) {

                // Write the closure header
                oFile << HEADER_CLOSURE_CSV;

            }

            // Curve file
            found = strFileName.find("_TM");
            if (found != std::string::npos) {

                // Write the curve header
                oFile << HEADER_CURVE_CSV;

            }

            // Write
            oFile << strLine;

        }
    
    } else { // The file exists already

        // Open
        ofstream oFile;
        oFile.open(strFileName, ios_base::app);
        
        // Check if the file is opened
        if (oFile.is_open()) {
            
            // Write
            oFile << strLine;

        }

    }

    strLine = "";

}

// Method that checks if the file exists already
bool File::checkFile(std::string strFileName) {

    ifstream iFile(strFileName);

    return (bool)iFile;

}