#include "../include/Date.h"

Date::Date() {

}

std::vector<unsigned char> Date::strDateToHex(std::string strDate) {

    // Date Format: dd/mm/yyyy hh:mm
    std::vector<unsigned char> vDate;

    // Year
    vDate.push_back((Helper::stringToInt(strDate.substr(6,4)) >> 8) & 0xFF); // First byte
    vDate.push_back((Helper::stringToInt(strDate.substr(6,4))) & 0xFF); // Second byte
    // Month
    vDate.push_back(Helper::stringToHex(strDate.substr(3,2)));
    // Day
    vDate.push_back(Helper::stringToHex(strDate.substr(0,2)));
    // Day of the week
    vDate.push_back(Helper::intToHex(dayOfTheWeek(Helper::stringToInt(strDate.substr(0,2)), Helper::stringToInt(strDate.substr(3,2)), Helper::stringToInt(strDate.substr(6,4)))));
    // Hour
    vDate.push_back(Helper::stringToHex(strDate.substr(11,2)));
    // Minute
    vDate.push_back(Helper::stringToHex(strDate.substr(14,2)));
    // Seconds
    vDate.push_back(0x00);

    return vDate;

}

std::string Date::hexDateToStr(std::vector<unsigned char> vDate) {

    // std::cout << "\nvDate: " << Helper::frameToString(vDate);

    std::string strDate = "";
    unsigned short year = vDate[2] << 8 | (vDate[3] & 0xFF);
    int month = (int) vDate[4];
    int day = (int) vDate[5];
    int hour = (int) vDate[7];
    int minute = (int) vDate[8];

    strDate.append( (day < 10) ? "0" + std::to_string(day) + "/" : std::to_string(day) + "/" );
    strDate.append( (month < 10) ? "0" + std::to_string(month) + "/" : std::to_string(month) + "/" );
    strDate.append(std::to_string(year) + " ");
    strDate.append( (hour < 10) ? "0" + std::to_string(hour) + ":" : std::to_string(hour) + ":" );
    strDate.append( (minute < 10) ? "0" + std::to_string(minute) : std::to_string(minute) );

    if (strDate.compare("255/255/65535 255:255") == 0) {

        strDate = "";

    }

    return strDate;

}

int Date::dayOfTheWeek(int day, int month, int year) {

    int dayOfTheWeek = -1;
    int modMonth = 0;

    if( (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0) ) {

        if (month == 1)    modMonth = 0; 
        if (month == 2)    modMonth = 3; 
        if (month == 3)    modMonth = 4; 
        if (month == 4)    modMonth = 0; 
        if (month == 5)    modMonth = 2; 
        if (month == 6)    modMonth = 5; 
        if (month == 7)    modMonth = 0; 
        if (month == 8)    modMonth = 3; 
        if (month == 9)    modMonth = 6; 
        if (month == 10)   modMonth = 1; 
        if (month == 11)   modMonth = 4; 
        if (month == 12)   modMonth = 6;

    } else {

        if (month == 1)    modMonth = 0; 
        if (month == 2)    modMonth = 3; 
        if (month == 3)    modMonth = 3; 
        if (month == 4)    modMonth = 6; 
        if (month == 5)    modMonth = 1; 
        if (month == 6)    modMonth = 4; 
        if (month == 7)    modMonth = 6; 
        if (month == 8)    modMonth = 2; 
        if (month == 9)    modMonth = 5; 
        if (month == 10)   modMonth = 0; 
        if (month == 11)   modMonth = 3; 
        if (month == 12)   modMonth = 5;

    }

    dayOfTheWeek = ( (year - 1) % 7 + ( (year - 1) / 4 - (3 * ( (year - 1) / 100 + 1) / 4) ) % 7 + modMonth + day % 7 ) % 7;

    return dayOfTheWeek; 

}

std::string Date::timestamp() {

    time_t timestamp_sec;
    time(&timestamp_sec);
    std::stringstream date;
    date << timestamp_sec;

    return date.str();

}

std::string Date::now() {
    
    std::stringstream ss;
    auto now = std::chrono::system_clock::now();
    auto time = std::chrono::system_clock::to_time_t(now);
    
    ss << std::put_time(localtime(&time), "%d/%m/%Y %H:%M:%S");

    return ss.str();

}

int Date::compare(std::string strDateA, std::string strDateB) {

    // strDateA format -> dd/mm/yyyy hh:mm
    // strDateB format -> dd/mm/yyyy hh:mm

    // Result codes:
    //  [0] -> strDateA > strDateB
    //  [1] -> strDateA = strDateB
    //  [2] -> strDateA < strDateB
    int result = -1;
    unsigned long long int uiDateA = 0;
    unsigned long long int uiDateB = 0;
    time_t curr_time;
    struct tm *tmDate;
    time(&curr_time);
    // auto now = std::chrono::system_clock::now();
    // auto time = std::chrono::system_clock::to_time_t(now);
	tmDate = localtime(&curr_time);
    // strDateA
    tmDate->tm_hour = Helper::stringToInt(strDateA.substr(11, 2)); // Hour
    tmDate->tm_min = Helper::stringToInt(strDateA.substr(14, 2)); // Minute
    tmDate->tm_mday = Helper::stringToInt(strDateA.substr(0, 2)); // Day 
    tmDate->tm_mon = Helper::stringToInt(strDateA.substr(3, 2)) - 1; // Month 
    tmDate->tm_year = Helper::stringToInt(strDateA.substr(6, 4)) - 1900; // Year
    uiDateA = mktime(tmDate);
    // strDateB
    tmDate->tm_hour = Helper::stringToInt(strDateB.substr(11, 2)); // Hour
    tmDate->tm_min = Helper::stringToInt(strDateB.substr(14, 2)); // Minute
    tmDate->tm_mday = Helper::stringToInt(strDateB.substr(0, 2)); // Day 
    tmDate->tm_mon = Helper::stringToInt(strDateB.substr(3, 2)) - 1; // Month 
    tmDate->tm_year = Helper::stringToInt(strDateB.substr(6, 4)) - 1900; // Year
    uiDateB = mktime(tmDate);    
    
    if (uiDateA > uiDateB) result = 0;
    else if (uiDateA == uiDateB) result = 1;
    else if (uiDateA < uiDateB) result = 2;

    return result;

}

std::string Date::nextDateToDownload(std::string strDate, int capturePeriod) {

    char date[20]; // dd/mm/yyyy
    char hour[20]; // hh:mm
    time_t curr_time;
    struct tm *tmDate;
    time(&curr_time);
    // auto now = std::chrono::system_clock::now();
    // auto time = std::chrono::system_clock::to_time_t(now);
	tmDate = localtime(&curr_time);
    std::string strResult = "";
    int addMinutes = 0;

    // strDate format: dd/mm/aaaa hh:mm

    // Convert strDate to tm
    tmDate->tm_hour = Helper::stringToInt(strDate.substr(11, 2)); // Hour
    int minute = Helper::stringToInt(strDate.substr(14, 2));
    tmDate->tm_min = minute; // Minute
    tmDate->tm_mday = Helper::stringToInt(strDate.substr(0, 2)); // Day
    tmDate->tm_mon = Helper::stringToInt(strDate.substr(3, 2)) - 1; // Month 
    tmDate->tm_year = Helper::stringToInt(strDate.substr(6, 4)) - 1900; // Year
    mktime(tmDate);


    if ( (minute == 0) || (minute == 15) || (minute == 30) || (minute == 45) ) {

        addMinutes = (capturePeriod == 15) ? 15 : 60;

    } else {

        if ( (minute > 0) && (minute < 15) ) {

            addMinutes = 15 - minute;

        } else if ( (minute > 15) && (minute < 30) ) {

            addMinutes = 30 - minute;

        } else if ( (minute > 30) && (minute < 45) ) {

            addMinutes = 45 - minute;

        } else if ( (minute > 45) && (minute < 60) ) {

            addMinutes = 60 - minute;

        }

    }

    tmDate->tm_min += addMinutes;
    mktime(tmDate);

    strftime(date, sizeof(date), "%d/%m/%Y", tmDate);
    strftime(hour, sizeof(hour), "%H:%M", tmDate);

    std::string strAuxDate(date);
    std::string strAuxHour(hour);
    strResult = strAuxDate + " " + strAuxHour;

    // TODO: Cambios de hora
    // Date::getDay(int year)

    return strResult;

}

int Date::getDay(int year) {

    bool search = true;
    int day = 28;

    if (this->vDays.empty() == true) {

        this->vDays.push_back(std::tuple<int,int>(2019,27));
        this->vDays.push_back(std::tuple<int,int>(2020,25));
        this->vDays.push_back(std::tuple<int,int>(2021,31));
        this->vDays.push_back(std::tuple<int,int>(2022,30));
        this->vDays.push_back(std::tuple<int,int>(2023,29));
        this->vDays.push_back(std::tuple<int,int>(2024,27));
        this->vDays.push_back(std::tuple<int,int>(2025,26));
        this->vDays.push_back(std::tuple<int,int>(2026,25));
        this->vDays.push_back(std::tuple<int,int>(2027,31));
        this->vDays.push_back(std::tuple<int,int>(2028,29));
        this->vDays.push_back(std::tuple<int,int>(2029,28));
        this->vDays.push_back(std::tuple<int,int>(2030,27));
        this->vDays.push_back(std::tuple<int,int>(2031,26));
        this->vDays.push_back(std::tuple<int,int>(2032,31));
        this->vDays.push_back(std::tuple<int,int>(2033,30));
        this->vDays.push_back(std::tuple<int,int>(2034,29));
        this->vDays.push_back(std::tuple<int,int>(2035,28));
        this->vDays.push_back(std::tuple<int,int>(2036,26));
        this->vDays.push_back(std::tuple<int,int>(2037,25));
        this->vDays.push_back(std::tuple<int,int>(2038,31));
        this->vDays.push_back(std::tuple<int,int>(2039,30));
        this->vDays.push_back(std::tuple<int,int>(2040,28));
        this->vDays.push_back(std::tuple<int,int>(2041,27));
        this->vDays.push_back(std::tuple<int,int>(2042,26));
        this->vDays.push_back(std::tuple<int,int>(2043,25));
        this->vDays.push_back(std::tuple<int,int>(2044,30));
        this->vDays.push_back(std::tuple<int,int>(2045,29));
        this->vDays.push_back(std::tuple<int,int>(2046,28));
        this->vDays.push_back(std::tuple<int,int>(2047,27));
        this->vDays.push_back(std::tuple<int,int>(2048,25));
        this->vDays.push_back(std::tuple<int,int>(2049,31));
        this->vDays.push_back(std::tuple<int,int>(2050,30));
        this->vDays.push_back(std::tuple<int,int>(2051,29));
        this->vDays.push_back(std::tuple<int,int>(2052,27));
        this->vDays.push_back(std::tuple<int,int>(2053,26));
        this->vDays.push_back(std::tuple<int,int>(2054,25));
        this->vDays.push_back(std::tuple<int,int>(2055,31));
        this->vDays.push_back(std::tuple<int,int>(2056,29));
        this->vDays.push_back(std::tuple<int,int>(2057,28));
        this->vDays.push_back(std::tuple<int,int>(2058,27));
        this->vDays.push_back(std::tuple<int,int>(2059,26));
        this->vDays.push_back(std::tuple<int,int>(2060,31));
        this->vDays.push_back(std::tuple<int,int>(2061,30));
        this->vDays.push_back(std::tuple<int,int>(2062,29));
        this->vDays.push_back(std::tuple<int,int>(2063,28));
        this->vDays.push_back(std::tuple<int,int>(2064,26));
        this->vDays.push_back(std::tuple<int,int>(2065,25));
        this->vDays.push_back(std::tuple<int,int>(2066,31));
        this->vDays.push_back(std::tuple<int,int>(2067,30));
        this->vDays.push_back(std::tuple<int,int>(2068,28));
        this->vDays.push_back(std::tuple<int,int>(2069,27));
        this->vDays.push_back(std::tuple<int,int>(2070,26));
        this->vDays.push_back(std::tuple<int,int>(2071,25));
        this->vDays.push_back(std::tuple<int,int>(2072,30));
        this->vDays.push_back(std::tuple<int,int>(2073,29));
        this->vDays.push_back(std::tuple<int,int>(2074,28));
        this->vDays.push_back(std::tuple<int,int>(2075,27));
        this->vDays.push_back(std::tuple<int,int>(2076,25));
        this->vDays.push_back(std::tuple<int,int>(2077,31));
        this->vDays.push_back(std::tuple<int,int>(2078,30));
        this->vDays.push_back(std::tuple<int,int>(2079,29));
        this->vDays.push_back(std::tuple<int,int>(2080,27));
        this->vDays.push_back(std::tuple<int,int>(2081,26));
        this->vDays.push_back(std::tuple<int,int>(2082,25));
        this->vDays.push_back(std::tuple<int,int>(2083,31));
        this->vDays.push_back(std::tuple<int,int>(2084,29));
        this->vDays.push_back(std::tuple<int,int>(2085,28));
        this->vDays.push_back(std::tuple<int,int>(2086,27));
        this->vDays.push_back(std::tuple<int,int>(2087,26));
        this->vDays.push_back(std::tuple<int,int>(2088,31));
        this->vDays.push_back(std::tuple<int,int>(2089,30));
        this->vDays.push_back(std::tuple<int,int>(2090,29));
        this->vDays.push_back(std::tuple<int,int>(2091,28));
        this->vDays.push_back(std::tuple<int,int>(2092,26));
        this->vDays.push_back(std::tuple<int,int>(2093,25));
        this->vDays.push_back(std::tuple<int,int>(2094,31));
        this->vDays.push_back(std::tuple<int,int>(2095,30));
        this->vDays.push_back(std::tuple<int,int>(2096,28));
        this->vDays.push_back(std::tuple<int,int>(2097,27));
        this->vDays.push_back(std::tuple<int,int>(2098,26));
        this->vDays.push_back(std::tuple<int,int>(2099,25));

    }

    for (std::size_t i = 0; (i < this->vDays.size()) && (search == true); i++) {
        
        if (year == std::get<0>(this->vDays.at(i))) {
    
            search = false;
            day = std::get<1>(this->vDays.at(i));
    
        }
        
    }

    return day;

}