#include "../include/Helper.h"

Helper::Helper() { }

std::string Helper::frameToString(std::vector<unsigned char> vFrame) {

    // Print the frame
    std::stringstream ss;
    ss  << "Frame length: [" + std::to_string(vFrame.size()) + "] : ";
    for (std::size_t i = 0; i < vFrame.size(); i++) {

        ss << "[" << i << "]";
        ss << std::hex << std::uppercase << std::setw(2) << std::setfill('0') << (unsigned int)vFrame[i];
        ss << std::dec << "(" << (unsigned int)vFrame[i] << ") ";

    }
    
    return ss.str();

}

int Helper::stringToInt(std::string str) {

    int numero = 0;

    try {   

        numero = std::stoi(str);

    } catch (std::invalid_argument& e){

        numero = -1;

    } catch (std::out_of_range& e) {

        numero = -1;

    }

    return numero;

}

// Method to convert an integer to hexadecimal
unsigned char Helper::intToHex(int number) {

    std::stringstream stream;

    stream << std::hex << number;
    // Convert to unsigned char
    union U
    {
        unsigned int valor;
        unsigned char componente;
    };
    U u;
    std::stringstream SS(stream.str());
    SS >> std::hex >> u.valor;
    
    return u.componente;

}

unsigned char Helper::stringToHex(std::string str) {

    return intToHex(stringToInt(str));

}
