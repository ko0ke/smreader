#include "../include/Controller.h"

Controller::Controller() : mode("") {

    this->logFile = "";
    // TODO: Fichero de Log exclusivo para cada comunicacion

}

void Controller::configure(int argc, char *argv[]) {

    bool stop = false;
    Serial auxSerial;
    Socket auxSocket;
    std::string auxStr = "";

    if (argc > 1) {

        if ( (strcmp(argv[1], "-h") == 0) || (strcmp(argv[1], "--help") == 0) ) {

            // TODO: Help message
            std::cout << "\nHELP";

        } else {

            for (int i = 1; (i < argc) && (stop == false); i++) {

                //     #########################
                //     ##  SOCKET CONNECTION  ##
                //     #########################

                if ( (strcmp(argv[i], "-socket") == 0) && ((i + 1) < argc) ) {

                    // TODO: 
                    // * Nombre de fichero -L
                    // * Nivel de debug -D

                    /*
                        -i -> IP
                        -p -> Port
                        -t -> Timeout
                        -P -> Protocol
                        -a -> Link address
                        -m -> Measurement point
                        -k -> Password
                        -L -> LogFile
                        -D -> Debug level
                        -r -> Request
                    */

                    i = i + 1;

                    std::stringstream ssParameters(argv[i]);
                    std::string strParameter = "";
                    
                    int p = 0;
                    while (getline(ssParameters, strParameter, '-')) { 
                        
                        //     ##########
                        //     ##  IP  ##
                        //     ##########

                        if (strParameter[p] == 'i') {

                            auxStr = this->getParameter(strParameter);

                            // std::cout << "ip...[" << auxStr << "]\n";

                            // TODO: Check if is a valid IP
                            auxSocket.setIp(auxStr);

                        }

                        //     ############
                        //     ##  PORT  ##
                        //     ############
                        
                        else if (strParameter[p] == 'p') {

                            auxStr = this->getParameter(strParameter);
                            
                            // std::cout << "port...[" << auxStr << "]\n";
                            
                            // TODO: Check if is a valid Port
                            auxSocket.setPort(Helper::stringToInt(auxStr));

                        }

                        //     ###############
                        //     ##  TIMEOUT  ##
                        //     ###############
                        
                        else if (strParameter[p] == 't') {

                            auxStr = this->getParameter(strParameter);

                            // std::cout << "timeout...[" << auxStr << "]\n";

                            // TODO: Check if is a valid Timeout
                            auxSocket.setTimeout(Helper::stringToInt(auxStr));

                        }

                        //     ################
                        //     ##  PROTOCOL  ##
                        //     ################
                        
                        else if (strParameter[p] == 'P') {

                            auxStr = this->getParameter(strParameter);

                            // std::cout << "protocol...[" << auxStr << "]\n";

                            // TODO: Check if is a valid Protocol
                            auxSocket.setProtocol(auxStr);

                        }

                        //     ####################
                        //     ##  LINK ADDRESS  ##
                        //     ####################
                        
                        else if (strParameter[p] == 'a') {

                            auxStr = this->getParameter(strParameter);

                            // std::cout << "link-address...[" << auxStr << "]\n";

                            // TODO: Check if is a valid LinkAddress
                            auxSocket.setLinkAddress(Helper::stringToInt(auxStr));

                        } 

                        //     #########################
                        //     ##  MEASUREMENT POINT  ##
                        //     #########################
                        
                        else if (strParameter[p] == 'm') {

                            auxStr = this->getParameter(strParameter);

                            // std::cout << "measurement-point...[" << auxStr << "]\n";

                            // TODO: Check if is a valid MeasurementPoint
                            auxSocket.setMeasurementPoint(Helper::stringToInt(auxStr));

                        } 

                        //     ################
                        //     ##  PASSWORD  ##
                        //     ################
                        
                        else if (strParameter[p] == 'k') {

                            auxStr = this->getParameter(strParameter);

                            // std::cout << "password...[" << auxStr << "]\n";

                            // TODO: Check if is a valid Password
                            auxSocket.setPassword(Helper::stringToInt(auxStr));

                        }

                        //     ################
                        //     ##  Log File  ##
                        //     ################
                        else if (strParameter[p] == 'L') {
                            
                            auxStr = this->getParameter(strParameter);

                            this->logFile = auxStr;

                        }

                        //     ###################
                        //     ##  DEBUG LEVEL  ##
                        //     ###################
                        else if (strParameter[p] == 'D') {



                        }

                        //     #################
                        //     ##  OPERATION  ##
                        //     #################

                        else if (strParameter[p] == 'o') {

                            auxStr = this->getParameter(strParameter);

                            // TODO: Check if is a valid operation
                            auxSocket.setOperation(auxStr);

                        }
                        
                        //     ###############
                        //     ##  REQUEST  ##
                        //     ###############

                        else if (strParameter[p] == 'r') {
                        
                            // -request "1|2|3 inc|abs|c1|c2|c3|t dd/mm/yyyy hh:mm dd/mm/yyyy hh:mm"

                            std::stringstream ssRequestParameters(this->getParameter(strParameter));
                            std::string strRequestParameter = "";
                            int curve = 0;
                            std::string curveType = "";
                            std::string initialDate = "";
                            std::string initialHour = "";
                            std::string finalDate = "";
                            std::string finalHour = "";
                            
                            int nParameter = 0;
                            while (getline(ssRequestParameters, strRequestParameter, ' ')) { 
                                
                                //     #############
                                //     ##  CURVE  ##
                                //     #############

                                if (nParameter == 0) {

                                    // TODO: Check if is a valid Curve
                                    curve = Helper::stringToInt(strRequestParameter);

                                } 
                                
                                //     ##################
                                //     ##  CURVE TYPE  ##
                                //     ##################
                                
                                else if (nParameter == 1) {

                                    // TODO: Check if is a valid CurveType
                                    curveType = strRequestParameter;

                                } 
                                
                                //     ####################
                                //     ##  INITIAL DATE  ##
                                //     ####################
                                
                                else if (nParameter == 2) {

                                    // TODO: Check if is a valid InitialDate
                                    initialDate = strRequestParameter;

                                } 

                                //     ####################
                                //     ##  INITIAL HOUR  ##
                                //     ####################
                                
                                else if (nParameter == 3) {

                                    // TODO: Check if is a valid InitialHour
                                    initialHour = strRequestParameter;

                                } 
                                
                                //     ##################
                                //     ##  FINAL DATE  ##
                                //     ##################

                                else if (nParameter == 4) {

                                    // TODO: Check if is a valid FinalDate
                                    finalDate = strRequestParameter;

                                } 
                                
                                //     ##################
                                //     ##  FINAL HOUR  ##
                                //     ##################
                                
                                else if (nParameter == 5) {

                                    // TODO: Check if is a valid FinalHour
                                    finalHour = strRequestParameter;

                                } 
                                
                                //     #############
                                //     ##  OTHER  ##
                                //     #############
                                
                                else {

                                    // Other...

                                }

                                nParameter = nParameter + 1;

                            }

                            // Create the logName for this request
                            if (this->logFile.compare("") == 0) {

                                this->logFile = Date::timestamp();

                            }
                            auxSocket.setLogFile(this->logFile);

                            // Add a new request
                            auxSocket.addRequest(curve, curveType, initialDate + " " + initialHour, finalDate + " " + finalHour);

                        }

                    }

                    this->vCommunication.push_back(std::unique_ptr<Communication>(new Socket(auxSocket)));

                    auxSocket.reset();

                    p = p + 1;

                }

                //     #########################
                //     ##  SERIAL CONNECTION  ##
                //     #########################

                else if ( (strcmp(argv[i], "-serial") == 0) && ((i + 1) < argc) ) {

                    i = i + 1;

                    // TODO: 
                    // * Nombre de fichero -L
                    // * Nivel de debug -D

                    /*
                        
                        -n -> phone number
                        -p -> COMM
                        -t -> Timeout
                        -P -> Protocol
                        -a -> Link address
                        -m -> Measurement point
                        -k -> Password
                        -L -> LogFile
                        -D -> Debug level
                        -r -> Request
                    */

                    std::stringstream ssParameters(argv[i]);
                    std::string strParameter = "";
                    
                    int p = 0;
                    while (getline(ssParameters, strParameter, '-')) { 

                        // std::cout << "strParameter: " << strParameter << "\n";
                        
                        //     ####################
                        //     ##  PHONE NUMBER  ##
                        //     ####################

                        if (strParameter[p] == 'n') {

                            auxStr = this->getParameter(strParameter);

                            // std::cout << "phone-number...[" << auxStr << "]\n";

                            // TODO: Check if is a valid phone-number
                            auxSerial.setPhoneNumber(auxStr);

                        }

                        //     ############
                        //     ##  COMM  ##
                        //     ############
                        
                        else if (strParameter[p] == 'p') {

                            auxStr = this->getParameter(strParameter);
                            
                            // std::cout << "port...[" << auxStr << "]\n";
                            
                            // TODO: Check if is a valid Port
                            auxSerial.setCOMM("\\\\.\\" + auxStr);

                        }

                        //     ###############
                        //     ##  TIMEOUT  ##
                        //     ###############
                        
                        else if (strParameter[p] == 't') {

                            auxStr = this->getParameter(strParameter);

                            // std::cout << "timeout...[" << auxStr << "]\n";

                            // TODO: Check if is a valid Timeout
                            auxSerial.setTimeout(Helper::stringToInt(auxStr));

                        }

                        //     ################
                        //     ##  PROTOCOL  ##
                        //     ################
                        
                        else if (strParameter[p] == 'P') {

                            auxStr = this->getParameter(strParameter);

                            // std::cout << "protocol...[" << auxStr << "]\n";

                            // TODO: Check if is a valid Protocol
                            auxSerial.setProtocol(auxStr);

                        }

                        //     ####################
                        //     ##  LINK ADDRESS  ##
                        //     ####################
                        
                        else if (strParameter[p] == 'a') {

                            auxStr = this->getParameter(strParameter);

                            // std::cout << "link-address...[" << auxStr << "]\n";

                            // TODO: Check if is a valid LinkAddress
                            auxSerial.setLinkAddress(Helper::stringToInt(auxStr));

                        } 

                        //     #########################
                        //     ##  MEASUREMENT POINT  ##
                        //     #########################
                        
                        else if (strParameter[p] == 'm') {

                            auxStr = this->getParameter(strParameter);

                            // std::cout << "measurement-point...[" << auxStr << "]\n";

                            // TODO: Check if is a valid MeasurementPoint
                            auxSerial.setMeasurementPoint(Helper::stringToInt(auxStr));

                        } 

                        //     ################
                        //     ##  PASSWORD  ##
                        //     ################
                        
                        else if (strParameter[p] == 'k') {

                            auxStr = this->getParameter(strParameter);

                            // std::cout << "password...[" << auxStr << "]\n";

                            // TODO: Check if is a valid Password
                            auxSerial.setPassword(Helper::stringToInt(auxStr));

                        } 

                        //     ################
                        //     ##  Log File  ##
                        //     ################
                        else if (strParameter[p] == 'L') {
                            
                            auxStr = this->getParameter(strParameter);

                            this->logFile = auxStr;

                        }

                        //     ###################
                        //     ##  DEBUG LEVEL  ##
                        //     ###################
                        else if (strParameter[p] == 'D') {



                        }

                        //     #################
                        //     ##  OPERATION  ##
                        //     #################

                        else if (strParameter[p] == 'o') {

                            auxStr = this->getParameter(strParameter);

                            // TODO: Check if is a valid operation
                            auxSerial.setOperation(auxStr);

                        }
                        
                        //     ###############
                        //     ##  REQUEST  ##
                        //     ###############

                        else if (strParameter[p] == 'r') {
                        
                            // -request "1|2|3 i|a|c1|c2|c3|t dd/mm/yyyy hh:mm dd/mm/yyyy hh:mm"

                            std::stringstream ssRequestParameters(this->getParameter(strParameter));
                            std::string strRequestParameter = "";
                            int curve = 0;
                            std::string curveType = "";
                            std::string initialDate = "";
                            std::string initialHour = "";
                            std::string finalDate = "";
                            std::string finalHour = "";
                            
                            int nParameter = 0;
                            while (getline(ssRequestParameters, strRequestParameter, ' ')) { 

                                // std::cout << strRequestParameter << "\n";
                                
                                //     #############
                                //     ##  CURVE  ##
                                //     #############

                                if (nParameter == 0) {

                                    // TODO: Check if is a valid Curve
                                    curve = Helper::stringToInt(strRequestParameter);

                                } 
                                
                                //     ##################
                                //     ##  CURVE TYPE  ##
                                //     ##################
                                
                                else if (nParameter == 1) {

                                    // TODO: Check if is a valid CurveType
                                    curveType = strRequestParameter;

                                } 
                                
                                //     ####################
                                //     ##  INITIAL DATE  ##
                                //     ####################
                                
                                else if (nParameter == 2) {

                                    // TODO: Check if is a valid InitialDate
                                    initialDate = strRequestParameter;

                                } 

                                //     ####################
                                //     ##  INITIAL HOUR  ##
                                //     ####################
                                
                                else if (nParameter == 3) {

                                    // TODO: Check if is a valid InitialHour
                                    initialHour = strRequestParameter;

                                } 
                                
                                //     ##################
                                //     ##  FINAL DATE  ##
                                //     ##################

                                else if (nParameter == 4) {

                                    // TODO: Check if is a valid FinalDate
                                    finalDate = strRequestParameter;

                                } 
                                
                                //     ##################
                                //     ##  FINAL HOUR  ##
                                //     ##################
                                
                                else if (nParameter == 5) {

                                    // TODO: Check if is a valid FinalHour
                                    finalHour = strRequestParameter;

                                } 
                                
                                //     #############
                                //     ##  OTHER  ##
                                //     #############
                                
                                else {

                                    // Other...

                                }

                                nParameter = nParameter + 1;

                            }
                            
                            // Create the logName for this request
                            if (this->logFile.compare("") == 0) {

                                this->logFile = Date::timestamp();

                            }

                            auxSerial.setLogFile(this->logFile);
                            
                            // Add a new request
                            auxSerial.addRequest(curve, curveType, initialDate + " " + initialHour, finalDate + " " + finalHour);

                        }

                    }

                    this->vCommunication.push_back(std::unique_ptr<Communication>(new Serial(auxSerial)));
                    
                    auxSerial.reset();

                    p = p + 1;

                }

                //     ############
                //     ##  MODE  ##
                //     ############

                else if ( (strcmp(argv[i], "-mode") == 0) && ((i + 1) < argc) ) {

                    i = i + 1;
                    // std::cout << "-mode...[" << argv[i] << "]\n";

                }

                //     ########################
                //     ##  WRONG PARAMETERS  ##
                //     ########################

                else if ((i + 1) >= argc) {

                    stop = true;
                    std::cout << "wrong parameters...\n";

                }

                //     ################################
                //     ##  PARAMETER NOT RECOGNIZED  ##
                //     ################################

                else {

                    stop = true;
                    std::cout << "parameter not recognized...\n";

                }


                std::cout << "\n";

            }

            std::string strResults = "";

            for (std::size_t i = 0; i < this->vCommunication.size(); i++) {

                //     #####################
                //     ##  DLMS PROTOCOL  ##
                //     #####################

                if (this->vCommunication[i]->getProtocol().compare("dlms") == 0) {

                    // DLMS Communication
                    DLMSController<Communication> dlmsController;
                    dlmsController.read(*this->vCommunication[i]);
                    strResults.append(dlmsController.getResults());

                    Sleep(5000);

                }

                //     ####################
                //     ##  IEC PROTOCOL  ##
                //     ####################

                else if (this->vCommunication[i]->getProtocol().compare("iec") == 0) {

                    // IEC Communication
                
                }

                //     #############
                //     ##  OTHER  ##
                //     #############

                else {

                    // TODO: Other...

                }

            }
            
            File::write(this->logFile + ".log", "\n Resultado: \n" + strResults);
            // std::cout << "\n Resultado: \n" + strResults;
            std::cout << strResults;

        }

    } else {

        // TODO: Help message
        std::cout << "\nHELP with HELP";

    }
    
}

std::string Controller::getParameter(std::string str) {

    std::string auxStr = "";

    auxStr = str.substr(2);
    if (auxStr[auxStr.length() - 1] == ' ') {

        auxStr = auxStr.substr(0, auxStr.length() - 1);

    }

    return auxStr;

}