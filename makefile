# COMPILER
CXX = g++
# COMPILER FLAGS
CXX_FLAGS = -g -std=c++11 -pedantic
# EXE
EXE = smreader
# MAIN CPP
MAIN_CPP = main.cpp
# BIN DIRECTORY
BIN_DIR = bin
# CPP FILES
CPP_FILES = $(wildcard src/*.cpp)
# OBJECTS
OBJECTS = $(CPP_FILES:.cpp=.o)
# OBJECTS DIRECTORY
OBJECTS_DIR = $(subst src,build,$(OBJECTS))
# PROJECTS DIRECTORY
# PROJECTS_DIR = Communication IEC-870-5-102 IEC-62056
#OBJECT FILES
OBJECT_FILES = $(wildcard build/*.o)

all: projects $(OBJECTS_DIR) exe
	@echo all $<

projects: 
	+$(MAKE) -C Communication
	+$(MAKE) -C IEC-870-5-102
	+$(MAKE) -C IEC-62056
	
build/%.o : src/%.cpp include/%.h
	$(CXX) $(CXX_FLAGS) -Wall -w -c -Iinclude -o $@ $<

exe: 
	$(CXX) $(CXX_FLAGS) -Iinclude $(MAIN_CPP) $(OBJECT_FILES) -o $(BIN_DIR)/$(EXE) -lws2_32
	
