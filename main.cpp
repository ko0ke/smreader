#include "include/Controller.h"

int main(int argc, char *argv[]) {

    // TODO:
    // // 1. Nombre de fichero identico al de IEC
    // // 2. Fichero de Log exclusivo para cada comunicacion
    // // 3. Revisar la curva con contadores ZIV
    // // 4. Remedio para evitar que se quede siempre en bucle
    // * 5. Control de huecos
    // // 6. Añadir en el resultado la hora del contador y del servidor
    // * 7. Cambios de hora
    // * 8. Añadir el resultado para las operaciones (-o)
    // * 9. Implementar la solicitud de instantaneos
    // * 10. Añadir en los parametros de entrada el "Nivel de debug" -D
    //
    

    Controller controller;

    controller.configure(argc, argv);

    return 0;
}